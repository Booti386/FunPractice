#!/bin/bash

infile="$1"
outfile="$2"
spigot_api_version_major="$3"
spigot_api_version_minor="$4"
spigot_api_version_release="$5"
tmpfile="$(mktemp)"

sed "s://#:#:g" "${infile}" > "${tmpfile}"
gcc -E -Wp,-undef,-nostdinc,-P,-CC -o "${outfile}" -x c "${tmpfile}" "-DMAKEVER(x,y,z)=(((x) * 0x10000) | ((y) * 0x100) | (z))" "-DSPIGOT_API_VERSION=MAKEVER(${spigot_api_version_major}, ${spigot_api_version_minor}, ${spigot_api_version_release})"
ret="$?"
rm "${tmpfile}"
exit "${ret}"
