/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

/**
 *
 * @author guillaume
 */
public enum GameType {
	NO_DEBUFF("No debuff"),
	BUILD_UHC("Build Uhc", false, true),
	BUILD_POT("Build pot", false, true),
	G_APPLE("G apple"),
	ARCHER("Archer"),
	IRON("Iron", false, true),
	HC_NO_ENCHANTS("HC no enchants"),
	SG("SG", false, true),
	RANKED_NO_DEBUFF("No debuff", true),
	RANKED_BUILD_UHC("Build Uhc", true, true),
	RANKED_BUILD_POT("Build pot", true, true),
	RANKED_G_APPLE("G apple", true),
	RANKED_ARCHER("Archer", true),
	RANKED_IRON("Iron", true, true);

	private final String name;
	private final boolean isRanked;
	private final boolean isBuildable;

	private GameType() {
		this(false);
	}

	private GameType(String name) {
		this(name, false);
	}

	private GameType(boolean is_ranked) {
		this.name = super.toString();
		this.isRanked = is_ranked;
		this.isBuildable = false;
	}

	private GameType(boolean is_ranked, boolean is_buildable) {
		this.name = super.toString();
		this.isRanked = is_ranked;
		this.isBuildable = is_buildable;
	}

	private GameType(String name, boolean is_ranked) {
		this.name = name;
		this.isRanked = is_ranked;
		this.isBuildable = false;
	}

	private GameType(String name, boolean is_ranked, boolean is_buildable) {
		this.name = name;
		this.isRanked = is_ranked;
		this.isBuildable = is_buildable;
	}

	@Override
	public String toString() {
		return this.name;
	}

	public boolean getRanked() {
		return this.isRanked;
	}

	public boolean getBuildable() {
		return this.isBuildable;
	}

	public static GameType valueOfNoExcept(String name) {
		try {
			return valueOf(name);
		} catch(IllegalArgumentException ex) {
			return null;
		}
	}

}
