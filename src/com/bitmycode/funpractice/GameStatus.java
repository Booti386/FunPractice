/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

/**
 *
 * @author guillaume
 */
public enum GameStatus {
	WAITING,
	LAUNCHING,
	PRELAUNCHED,
	ABORTED_0,
	ABORTED_1,
	LAUNCHED,
	COMPLETED
}
