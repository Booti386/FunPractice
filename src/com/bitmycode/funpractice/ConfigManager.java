/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author guillaume
 */
public class ConfigManager {

	private final FunPractice plugin;
	private final Map<ArenaMeta, Game> arenas;
	private final Map<GameType, KitMeta> kits;
	private final Map<UUID, GamePlayerMeta> players;
	private final Map<RankedMatchesLimitType, Integer> maxRankedMatches;
	private boolean rankedEnabled;
	private int maxLayerUpdatesPerTick;
	private double playerMinY;
	private int arenaMinY;
	private int arenaMaxY;
	private int maxCompletedGames;
	private SpawnMeta mapSpawn;
	private SpawnMeta kitEditZoneSpawn;

	public ConfigManager(FunPractice plugin) {
		this.plugin = plugin;
		this.arenas = new HashMap<>();
		this.kits = new HashMap<>();
		this.players = new HashMap<>();
		this.maxRankedMatches = new HashMap<>();
		this.reloadConfig();
		this.saveConfig(true, false, false, false);
	}

	private boolean createConfigFileIfNotExist(File config_file) {
		final String sep = File.separator;
		String config_file_name = config_file.getName();

		if(!config_file.exists()) {
			try {
				InputStream is;
				String template_contents;
				PrintWriter writer;

				is = this.plugin.getResource("templates" + sep + config_file_name);
				if(is == null)
					throw new IOException("Failed to open the config file template $$jar$$" + sep + "templates" + sep + config_file_name);

				template_contents = new BufferedReader(new InputStreamReader(is)).lines().collect(Collectors.joining("\n"));

				config_file.createNewFile();
				writer = new PrintWriter(config_file);
				writer.write(template_contents);
				writer.close();
			} catch(IOException ex) {
				this.plugin.error("Failed to create the config file " + config_file.getPath() + ": ", ex);
				return false;
			}
		}
		return true;
	}

	private void setDefaultConfigOptions(YamlConfiguration config) {
		config.options().indent(4);
	}

	private void reloadConfig() {
		File config_file;
		YamlConfiguration config;
		ConfigurationSection kit_edit_zone_spawn_section;
		ConfigurationSection map_spawn_section;
		ConfigurationSection max_ranked_matches_section;
		File arenas_config_file;
		YamlConfiguration arenas_config;
		File kits_config_file;
		YamlConfiguration kits_config;

		this.plugin.log("Loading configuration...");

		config_file = new File(this.plugin.getDataFolder(), "config.yml");
		if(createConfigFileIfNotExist(config_file))
			config = YamlConfiguration.loadConfiguration(config_file);
		else
			config = new YamlConfiguration();

		this.rankedEnabled = config.getBoolean("RankedEnabled", true);
		this.plugin.log("Ranked enabled: " + this.rankedEnabled);

		this.maxLayerUpdatesPerTick = config.getInt("MaxLayerUpdatesPerTick", 1);
		this.plugin.log("Max layer updates per tick: " + this.maxLayerUpdatesPerTick);

		this.playerMinY = config.getDouble("PlayerMinY", 0.0);
		this.plugin.log("Player min Y: " + this.playerMinY);

		this.arenaMinY = config.getInt("ArenaMinY", 0);
		this.plugin.log("Arena min Y: " + this.arenaMinY);

		this.arenaMaxY = config.getInt("ArenaMaxY", 256);
		this.plugin.log("Arena max Y: " + this.arenaMaxY);

		this.maxCompletedGames = config.getInt("MaxCompletedGames", 100);
		this.plugin.log("Max completed games: " + this.maxCompletedGames);

		kit_edit_zone_spawn_section = config.getConfigurationSection("KitEditZoneSpawn");
		this.kitEditZoneSpawn = SpawnMeta.deserialize(kit_edit_zone_spawn_section);
		this.plugin.log("Kit edit zone spawn: " + this.kitEditZoneSpawn.toString());

		map_spawn_section = config.getConfigurationSection("MapSpawn");
		this.mapSpawn = SpawnMeta.deserialize(map_spawn_section);
		this.plugin.log("Map spawn: " + this.mapSpawn.toString());

		max_ranked_matches_section = config.getConfigurationSection("MaxRankedMatches");

		for (RankedMatchesLimitType type : RankedMatchesLimitType.values())
		{
			String name = type.name();
			int value = 0;

			if (max_ranked_matches_section != null)
				value = max_ranked_matches_section.getInt(name, 0);

			this.maxRankedMatches.put(type, value);
			this.plugin.log("MaxRankedMatches." + name + ": " + value);
		}

		this.plugin.log("Loading arenas...");

		arenas_config_file = new File(this.plugin.getDataFolder(), "arenas.yml");
		if(createConfigFileIfNotExist(arenas_config_file))
			arenas_config = YamlConfiguration.loadConfiguration(arenas_config_file);
		else
			arenas_config = new YamlConfiguration();

		arenas_config.getKeys(false).forEach((arena_name) -> {
			ArenaMeta meta = ArenaMeta.deserialize(arenas_config.getConfigurationSection(arena_name));
			ConfigManager.this.plugin.log(meta.toString());
			ConfigManager.this.arenas.put(meta, null);
		});

		this.plugin.log("Loading kits...");

		kits_config_file = new File(this.plugin.getDataFolder(), "kits.yml");
		if(createConfigFileIfNotExist(kits_config_file))
			kits_config = YamlConfiguration.loadConfiguration(kits_config_file);
		else
			kits_config = new YamlConfiguration();

		Arrays.asList(GameType.values()).forEach((game_type) -> {
			String game_type_name = game_type.name();
			ConfigurationSection kit_config = kits_config.getConfigurationSection(game_type_name);
			KitMeta meta = null;

			if(kit_config != null)
				meta = KitMeta.deserialize(kit_config);

			if(meta == null)
				meta = new KitMeta();

			ConfigManager.this.plugin.log(game_type.name() + ": " + meta.toString());
			ConfigManager.this.kits.put(game_type, meta);
		});

		new File(this.plugin.getDataFolder(), "players").mkdirs();

		saveConfig();
	}

	public void saveConfig(GamePlayerMeta player) {
		File player_config_file;
		YamlConfiguration player_config;
		ConfigurationSection section;

		player_config_file = new File(this.plugin.getDataFolder(), "players" + File.separator + player.getUUID() + ".yml");
		player_config = new YamlConfiguration();
		setDefaultConfigOptions(player_config);

		section = player.serialize();
		player_config.createSection(section.getName(), section.getValues(false));
		try {
			player_config.save(player_config_file);
		} catch(IOException ex) {
			this.plugin.error("Failed to save the config file " + player_config_file.getName() + ":", ex);
		}
	}

	public final void saveConfig(boolean save_config, boolean save_arenas, boolean save_kits, boolean save_players) {
		if(save_config) {
			File config_file = new File(this.plugin.getDataFolder(), "config.yml");
			YamlConfiguration config = new YamlConfiguration();
			final ConfigurationSection max_ranked_matches_section;
			setDefaultConfigOptions(config);

			config.set("RankedEnabled", this.rankedEnabled);
			config.set("MaxLayerUpdatesPerTick", this.maxLayerUpdatesPerTick);
			config.set("PlayerMinY", this.playerMinY);
			config.set("ArenaMinY", this.arenaMinY);
			config.set("ArenaMaxY", this.arenaMaxY);
			config.set("MaxCompletedGames", this.maxCompletedGames);
			config.createSection("KitEditZoneSpawn", this.kitEditZoneSpawn.serialize().getValues(false));
			config.createSection("MapSpawn", this.mapSpawn.serialize().getValues(false));
			max_ranked_matches_section = config.createSection("MaxRankedMatches");
			this.maxRankedMatches.forEach((type, limit) -> {
				max_ranked_matches_section.set(type.name(), limit);
			});
			try {
				config.save(config_file);
			} catch(IOException ex) {
				this.plugin.error("Failed to save the config file " + config_file.getName() + ":", ex);
			}
		}

		if(save_arenas) {
			File arenas_config_file = new File(this.plugin.getDataFolder(), "arenas.yml");
			YamlConfiguration arenas_config = new YamlConfiguration();
			setDefaultConfigOptions(arenas_config);

			this.arenas.forEach((arena, game) -> {
				ConfigurationSection section_ = arena.serialize();
				arenas_config.createSection(section_.getName(), section_.getValues(false));
			});
			try {
				arenas_config.save(arenas_config_file);
			} catch(IOException ex) {
				this.plugin.error("Failed to save the config file " + arenas_config_file.getName() + ":", ex);
			}
		}

		if(save_kits) {
			File kits_config_file = new File(this.plugin.getDataFolder(), "kits.yml");
			YamlConfiguration kits_config = new YamlConfiguration();
			setDefaultConfigOptions(kits_config);

			this.kits.forEach((game_type, kit) -> {
				ConfigurationSection section_ = kit.serialize();
				kits_config.createSection(game_type.name(), section_.getValues(false));
			});
			try {
				kits_config.save(kits_config_file);
			} catch(IOException ex) {
				this.plugin.error("Failed to save the config file " + kits_config_file.getName() + ":", ex);
			}
		}

		if(save_players) {
			this.players.forEach((uuid, player) -> {
				saveConfig(player);
			});
		}
	}

	public void saveConfig() {
		saveConfig(true, true, true, true);
	}

	public boolean getRankedEnabled() {
		return this.rankedEnabled;
	}

	public int getMaxLayerUpdatesPerTick() {
		return this.maxLayerUpdatesPerTick;
	}

	public double getPlayerMinY() {
		return this.playerMinY;
	}

	public int getArenaMinY() {
		return this.arenaMinY;
	}

	public int getArenaMaxY() {
		return this.arenaMaxY;
	}

	public int getMaxCompletedGames() {
		return this.maxCompletedGames;
	}

	public SpawnMeta getKitEditZoneSpawn() {
		return this.kitEditZoneSpawn;
	}

	public SpawnMeta getMapSpawn() {
		return this.mapSpawn;
	}

	public final List<ArenaMeta> getArenas() {
		return this.arenas.keySet().stream()
				.collect(Collectors.toList());
	}

	public List<ArenaMeta> getFreeArenas() {
		return this.arenas.entrySet().stream()
				.filter((entry) -> entry.getValue() == null)
				.map((entry) -> entry.getKey())
				.collect(Collectors.toList());
	}

	public ArenaMeta getRandomFreeArena() {
		List<ArenaMeta> m = getFreeArenas();

		if (m.isEmpty())
			return null;

		return m.get(new Random().nextInt(m.size()));
	}

	public int getArenaCount() {
		return this.arenas.size();
	}

	public int getFreeArenaCount() {
		return (int)this.arenas.entrySet().stream()
				.filter((entry) -> entry.getValue() == null)
				.count();
	}

	public KitMeta getKit(GameType game_type) {
		return this.kits.get(game_type);
	}

	public void replaceKit(GameType game_type, KitMeta kit) {
		this.kits.put(game_type, kit);
	}

	public GamePlayerMeta createPlayerIfNotExists(UUID uuid) {
		GamePlayerMeta meta = this.players.get(uuid);

		if(meta == null) {
			File player_config_file = new File(this.plugin.getDataFolder(), "players" + File.separator + uuid + ".yml");
			YamlConfiguration player_config;
			ConfigurationSection player_section = null;

			try {
				player_config_file.createNewFile();
			} catch (IOException ex) {}
			player_config = YamlConfiguration.loadConfiguration(player_config_file);

			if(player_config != null)
				player_section = player_config.getConfigurationSection(uuid.toString());

			if(player_section != null)
				meta = GamePlayerMeta.deserialize(player_section);

			if(meta != null) {
				if(meta.getUUID().compareTo(uuid) == 0) {
					this.players.put(uuid, meta);
					saveConfig(meta);
				} else {
					player_config_file.delete();
					meta = null;
				}
			}
		}

		if(meta == null) {
			meta = new GamePlayerMeta(uuid);

			GamePlayerMeta meta_ = meta;
			Arrays.stream(GameType.values()).forEach((game_type) -> {
				meta_.setKit(game_type, this.getKit(game_type));
			});
			this.players.put(uuid, meta);
			saveConfig(meta);
		}
		return meta;
	}

	public int getMaxRankedMatches(RankedMatchesLimitType type)
	{
		return this.maxRankedMatches.get(type);
	}

	/* Non-getter/setter functions */

	public boolean attachGame(ArenaMeta arena, Game game) {
		if(arena == null)
			return false;
		return this.arenas.putIfAbsent(arena, game) == null;
	}

	public ArenaMeta attachGame(Game game) {
		ArenaMeta arena = getRandomFreeArena();
		return attachGame(arena, game) ? arena : null;
	}

	public void detachGame(Game game) {
		this.arenas.entrySet().stream()
				.filter((entry) -> entry.getValue() == game)
				.forEach((entry) -> {
					ArenaMeta arena = entry.getKey();

					if(game.getType().getBuildable())
						arena.setNeedsFullCleanup(true);
					this.arenas.put(arena, null);
				});
	}

}
