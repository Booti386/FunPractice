/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 *
 * @author guillaume
 */
public class DuelManager {

	private final FunPractice plugin;
	private int id;
	private final ConcurrentMap<Integer, Duel> duels;
	private final ConcurrentMap<GamePlayer, Duel> cached_duels;

	public DuelManager(FunPractice plugin) {
		this.plugin = plugin;
		this.id = 0;
		this.duels = new ConcurrentHashMap<>();
		this.cached_duels = new ConcurrentHashMap<>();
	}

	public int addDuel(Duel duel) {
		int duel_id = this.id++;

		duel.setTS(new Date().getTime());

		this.duels.put(duel_id, duel);

		removeExpiredDuels(10 * 60 * 1000);

		return duel_id;
	}

	public Duel getDuel(int id) {
		return this.duels.get(id);
	}

	public int getDuelCount() {
		return this.duels.size();
	}

	public void removeDuel(int id) {
		this.duels.remove(id);
	}

	public void addCachedDuel(GamePlayer player, Duel duel) {
		this.cached_duels.put(player, duel);
	}

	public Duel getCachedDuel(GamePlayer player) {
		return this.cached_duels.get(player);
	}

	public void removeCachedDuel(GamePlayer player) {
		this.cached_duels.remove(player);
	}

	public void removeExpiredDuels(long delta_ts) {
		long ts = new Date().getTime();

		this.duels.entrySet().stream()
				.filter((entry) -> ts - entry.getValue().getTS() > delta_ts)
				.forEach((entry) -> {
					this.removeDuel(entry.getKey());
				});
	}

}
