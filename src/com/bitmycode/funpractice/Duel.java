/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

/**
 *
 * @author guillaume
 */
public class Duel {

	final private GameType type;
	final private GamePlayer asker, target;
	private long ts;

	public Duel(GameType type, GamePlayer asker, GamePlayer target) {
		this.type = type;
		this.asker = asker;
		this.target = target;
		this.ts = -1;
	}

	public GameType getType() {
		return this.type;
	}

	public GamePlayer getAsker() {
		return this.asker;
	}

	public GamePlayer getTarget() {
		return this.target;
	}

	public long getTS() {
		return this.ts;
	}

	public void setTS(long ts) {
		this.ts = ts;
	}

}
