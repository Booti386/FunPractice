/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import java.util.NoSuchElementException;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockMultiPlaceEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.projectiles.ProjectileSource;

/**
 *
 * @author guillaume
 */
public class EventListener implements Listener {

	private final FunPractice plugin;

	public EventListener(FunPractice plugin) {
		this.plugin = plugin;
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerJoin(PlayerJoinEvent evt) {
		GamePlayer player;

		player = this.plugin.getPlayerManager().addPlayer(evt.getPlayer());
		player.resetState();
		player.teleportToSpawn();
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerQuit(PlayerQuitEvent evt) {
		Player p = evt.getPlayer();
		GamePlayer player;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		this.plugin.getGameManager().removePlayer(player);
		this.plugin.getPlayerManager().removePlayer(p);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onGameInitInventoryRightClick(PlayerInteractEvent evt) {
		Action action = evt.getAction();
		ItemStack item = evt.getItem();
		Player p = evt.getPlayer();
		GamePlayer player;
		boolean is_ranked;
		GameInventory open_inventory;

		if(action != Action.RIGHT_CLICK_BLOCK
				&& action != Action.RIGHT_CLICK_AIR)
			return;

		if(item == null)
			return;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		open_inventory = player.getOpenInventory();
		if(open_inventory == null)
			return;

		if(open_inventory.getType() != GameInventoryType.GAME_INIT)
			return;

		evt.setCancelled(true);

		switch(item.getType()) {
			case IRON_SWORD:
				is_ranked = false;
				break;

			case DIAMOND_SWORD:
				if (!plugin.getConfigManager().getRankedEnabled())
				{
					player.sendMessage("Combat classé désactivé sur ce serveur.");
					return;
				}

				if (!player.isRankedAllowed())
				{
					player.sendMessage("Limite maximale de combat classé atteinte.");
					return;
				}

				is_ranked = true;
				break;

			case INK_SACK:
				p.performCommand("duel-toggle");
				Bukkit.getScheduler().runTask(plugin, () -> {
					player.resetState();
				});
				return;

			case FEATHER:
				p.performCommand("fly-toggle");
				Bukkit.getScheduler().runTask(plugin, () -> {
					player.resetState();
				});
				return;

			case ANVIL:
				if(player.getStatus() != GamePlayerStatus.IDLE) {
					player.sendMessage("Vous ne pouvez pas effectuer cette action pour le moment.");
					return;
				}

				Bukkit.getScheduler().runTask(plugin, () -> {
					player.closeInventory(() -> {
						player.openInventory(new GameKitSelectInventory(this.plugin, player));
					});
				});
				return;

			default:
				return;
		}

		if(player.getStatus() == GamePlayerStatus.WAITING_GAME) {
			player.sendMessage("Vous êtes déjà en attente du jeu " + player.getGame().getType() + ".");
			return;
		}

		if(player.getStatus() == GamePlayerStatus.PLAYING) {
			player.sendMessage("Vous êtes déjà en train de jouer au jeu " + player.getGame().getType() + ".");
			return;
		}

		Bukkit.getScheduler().runTask(plugin, () -> {
			player.closeInventory(() -> {
				player.openInventory(new GameSelectInventory(this.plugin, player, is_ranked));
			});
		});
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onGameSelectInventoryClick(InventoryClickEvent evt) {
		HumanEntity human = evt.getWhoClicked();
		int item_slot = evt.getSlot();
		Player p;
		GamePlayer player;
		GameInventory open_inventory;
		GameSelectInventory inventory;
		GameType game_type;

		if(!(human instanceof Player))
			return;
		p = (Player)human;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		open_inventory = player.getOpenInventory();
		if(open_inventory == null)
			return;

		inventory = GameSelectInventory.fromInventory(open_inventory);
		if(inventory == null)
			return;

		evt.setCancelled(true);

		game_type = inventory.getGameType(item_slot);
		if(game_type == null)
			return;

		if(player.getStatus() == GamePlayerStatus.SELECTING_DUEL) {
			final Duel cached_duel = this.plugin.getDuelManager().getCachedDuel(player);

			if(cached_duel != null) {
				Bukkit.getScheduler().runTask(plugin, () -> {
					player.closeInventory(() -> {
						Duel duel;
						int id;
						TextComponent t;

						player.setStatus(GamePlayerStatus.IDLE);

						duel = new Duel(game_type, cached_duel.getAsker(), cached_duel.getTarget());
						id = this.plugin.getDuelManager().addDuel(duel);
						this.plugin.getDuelManager().removeCachedDuel(player);

						t = new TextComponent("§c" + duel.getAsker().getName() + "§7 vous a demandé en duel en §c" + game_type.toString());
						t.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/duel-accept " + id));
						t.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[] { new TextComponent("§aAccepter") }));
						duel.getTarget().sendMessage(t);
						player.resetState();
					});
				});
			}
		} else {
			Bukkit.getScheduler().runTask(plugin, () -> {
				player.closeInventory(() -> {
					this.plugin.getGameManager().registerPlayer(game_type, player);
				});
			});
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onGameLeaveInventoryRightClick(PlayerInteractEvent evt) {
		Action action = evt.getAction();
		ItemStack item = evt.getItem();
		Player p = evt.getPlayer();
		GamePlayer player;
		GameInventory open_inventory;

		if(action != Action.RIGHT_CLICK_BLOCK
				&& action != Action.RIGHT_CLICK_AIR)
			return;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		open_inventory = player.getOpenInventory();
		if(open_inventory == null)
			return;

		if(open_inventory.getType() != GameInventoryType.GAME_LEAVE)
			return;

		evt.setCancelled(true);

		if(item == null)
			return;

		if(item.getType() != Material.INK_SACK)
			return;

		Bukkit.getScheduler().runTask(plugin, () -> {
			player.closeInventory(() -> {
				this.plugin.getGameManager().removePlayer(player);
			});
		});
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onGameKitSelectInventoryClick(InventoryClickEvent evt) {
		HumanEntity human = evt.getWhoClicked();
		int item_slot = evt.getSlot();
		Player p;
		GamePlayer player;
		GameInventory open_inventory;
		GameKitSelectInventory inventory;
		GameType game_type;

		if(!(human instanceof Player))
			return;
		p = (Player)human;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		open_inventory = player.getOpenInventory();
		if(open_inventory == null)
			return;

		inventory = GameKitSelectInventory.fromInventory(open_inventory);
		if(inventory == null)
			return;

		evt.setCancelled(true);

		game_type = inventory.getGameType(item_slot);
		if(game_type == null)
			return;

		Bukkit.getScheduler().runTask(plugin, () -> {
			player.closeInventory(() -> {
				player.setStatus(GamePlayerStatus.EDITING_KIT);
				player.setFlyEnabled(false);
				player.setEditKitType(game_type);
				player.setInventory(player.getKit(game_type));
				player.teleport(this.plugin.getConfigManager().getKitEditZoneSpawn());
				player.sendMessage("§fVous avez été téléporté à l'édition de kits pour éditer le mode de jeu : §6" + game_type);
			});
		});
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onGameInventoryClick(InventoryClickEvent evt) {
		HumanEntity human = evt.getWhoClicked();
		Player p;
		GamePlayer player;
		GameInventory open_inventory;

		if(!(human instanceof Player))
			return;
		p = (Player)human;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		open_inventory = player.getOpenInventory();
		if(open_inventory == null)
			return;

		if(open_inventory.getType() != GameInventoryType.GAME_INIT
				&& open_inventory.getType() != GameInventoryType.GAME_LEAVE
				&& open_inventory.getType() != GameInventoryType.GAME_END)
			return;

		evt.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onGameInventoryClose(InventoryCloseEvent evt) {
		HumanEntity human = evt.getPlayer();
		Player p;
		GamePlayer player;
		GameInventory open_inventory;
		boolean reset = false;

		if(!(human instanceof Player))
			return;
		p = (Player)human;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		open_inventory = player.getOpenInventory();
		if(open_inventory == null)
			return;

		/* Echap */
		if(!open_inventory.getAnticipatedClose()) {
			if(open_inventory.getReplacePlayerInventory())
				return;
			reset = true;
		}

		final boolean do_reset = reset;
		Bukkit.getScheduler().runTask(plugin, () -> {
			player.doCloseInventory();
			if(do_reset)
				player.resetState();
		});
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onGameInventoryDrag(InventoryDragEvent evt) {
		HumanEntity human = evt.getWhoClicked();
		Player p;
		GamePlayer player;
		GameInventory open_inventory;

		if(!(human instanceof Player))
			return;
		p = (Player)human;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		open_inventory = player.getOpenInventory();
		if(open_inventory == null)
			return;

		evt.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onNonPlayingPlayerDamage(EntityDamageEvent evt) {
		Entity entity = evt.getEntity();
		Player p;
		GamePlayer player;

		if(!(entity instanceof Player))
			return;
		p = (Player)entity;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		if(player.getStatus() == GamePlayerStatus.PLAYING)
			return;

		evt.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayingPlayerDie(EntityDamageEvent evt) {
		Entity entity = evt.getEntity();
		Player p;
		GamePlayer player;
		double health;

		if(!(entity instanceof Player))
			return;
		p = (Player)entity;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		if(player.getStatus() != GamePlayerStatus.PLAYING)
			return;

		health = p.getHealth() - evt.getFinalDamage();
		if(health > 0)
			return;

		this.plugin.getGameManager().killPlayer(player);

		evt.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerDamageBlock(BlockDamageEvent evt) {
		Player p = evt.getPlayer();
		GamePlayer player;
		Game game;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		game = player.getGame();
		if(player.getStatus() == GamePlayerStatus.PLAYING && game != null) {
			if(game.getType().getBuildable()) {
				try
				{
					MetadataValue metadata = evt.getBlock().getMetadata("FunPractice.WasPutByPlayer").stream()
							.filter((value) -> value.getOwningPlugin() == this.plugin)
							.findAny()
							.get();
					if(metadata.asBoolean())
						return;
				} catch(NoSuchElementException ex) {}
			}
		}

		evt.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerBreakBlock(BlockBreakEvent evt) {
		Player player = evt.getPlayer();
		BlockDamageEvent sub_evt = new BlockDamageEvent(player, evt.getBlock(), Utils.getPlayerItemInMainHand(player), true);

		onPlayerDamageBlock(sub_evt);
		evt.setCancelled(sub_evt.isCancelled());
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerPutBlock(BlockPlaceEvent evt) {
		Player p = evt.getPlayer();
		GamePlayer player;
		Game game;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		game = player.getGame();
		if(player.getStatus() == GamePlayerStatus.PLAYING && game != null) {
			if(game.getType().getBuildable()) {
				evt.getBlock().setMetadata("FunPractice.WasPutByPlayer", new FixedMetadataValue(this.plugin, true));
				return;
			}
		}

		evt.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerPutMultiBlocks(BlockMultiPlaceEvent evt) {
		evt.getReplacedBlockStates().forEach((orig_state) -> {
			onPlayerPutBlock(new BlockPlaceEvent(orig_state.getBlock(), orig_state, evt.getBlockAgainst(), evt.getItemInHand(), evt.getPlayer(), true));
		});
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerEmptyBucket(PlayerBucketEmptyEvent evt) {
		BlockState orig_state = evt.getBlockClicked().getRelative(evt.getBlockFace()).getState();
		onPlayerPutBlock(new BlockPlaceEvent(orig_state.getBlock(), orig_state, evt.getBlockClicked(), evt.getItemStack(), evt.getPlayer(), true));
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerDropItem(PlayerDropItemEvent evt) {
		Player p = evt.getPlayer();
		GamePlayer player;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		if(player.getStatus() == GamePlayerStatus.EDITING_KIT) {
			evt.getItemDrop().remove();
			return;
		}

		evt.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerInteract(PlayerInteractEvent evt) {
		Player p = evt.getPlayer();
		GamePlayer player;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		if(evt.getAction() != Action.RIGHT_CLICK_BLOCK
				&& evt.getAction() != Action.LEFT_CLICK_BLOCK
				&& evt.getAction() != Action.RIGHT_CLICK_AIR
				&& evt.getAction() != Action.LEFT_CLICK_AIR)
			return;

		if(player.getStatus() == GamePlayerStatus.PLAYING)
			return;

		evt.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onDetachedPlayerInteract(PlayerInteractEvent evt) {
		Player p = evt.getPlayer();
		GamePlayer player;
		ItemStack item;
		Block block;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player != null)
			return;

		if(evt.getAction() != Action.RIGHT_CLICK_BLOCK)
			return;

		item = evt.getItem();
		if(item == null)
			return;

		if(item.getType() != Material.STICK
				|| !"§aCoordonnées du bloc".equals(item.getItemMeta().getDisplayName()))
			return;

		block = evt.getClickedBlock();
		if(block == null)
			return;

		p.sendMessage("World: " + block.getWorld().getName()
				+ "\nFace: " + evt.getBlockFace().toString()
				+ "\nX: " + block.getX()
				+ "\nY: " + block.getY()
				+ "\nZ: " + block.getZ());
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onEditKitZoneRightClick(PlayerInteractEvent evt) {
		Action action = evt.getAction();
		BlockState state;
		Sign sign;
		Player p = evt.getPlayer();
		GamePlayer player;
		GameType kit_type;

		if(action != Action.RIGHT_CLICK_BLOCK)
			return;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		if(player.getStatus() != GamePlayerStatus.EDITING_KIT)
			return;

		evt.setCancelled(true);

		state = evt.getClickedBlock().getState();

		kit_type = player.getEditKitType();

		switch(state.getType()) {
			case SIGN_POST:
			case WALL_SIGN:
				if(!(state instanceof Sign))
					break;
				sign = (Sign)state;

				if(!"[EditKit]".equals(sign.getLine(0)))
					break;

				switch(sign.getLine(2)) {
					case "Quit":
						player.resetState();
						player.teleportToSpawn();
						break;

					case "Save":
						player.setKit(kit_type, new KitMeta(p.getInventory()));
						player.sendMessage("Inventaire sauvegardé.");
						break;

					case "Reset":
						player.setKit(kit_type, this.plugin.getConfigManager().getKit(kit_type));
						player.setInventory(player.getKit(kit_type));
						player.sendMessage("Inventaire réinitialisé.");
						break;
				}
				break;

			default:
				break;
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerPotionDrink(PlayerItemConsumeEvent evt) {
		Player p = evt.getPlayer();
		GamePlayer player;
		PlayerInventory inventory;
		ItemStack item;
		ItemMeta item_meta;
		PotionMeta meta;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		if(player.getStatus() != GamePlayerStatus.PLAYING)
			return;

		item = evt.getItem();
		if(item.getType() != Material.POTION)
			return;

		item_meta = item.getItemMeta();
		if(!(item_meta instanceof PotionMeta))
			return;
		meta = (PotionMeta)item_meta;

		inventory = p.getInventory();

		Utils.getPotionEffectsFromItemStack(item).stream().forEach((effect) -> {
			p.addPotionEffect(effect, true);
		});
		if(meta.hasCustomEffects()) {
			meta.getCustomEffects().stream().forEach((effect) -> {
				p.addPotionEffect(effect, true);
			});
		}
		item.setAmount(item.getAmount() - 1);
		if(item.getAmount() <= 0)
			item.setType(Material.AIR);

		inventory.setItem(inventory.getHeldItemSlot(), item);
		p.updateInventory();
		evt.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onArrowDamage(EntityDamageByEntityEvent evt) {
		Entity entity = evt.getEntity();
		ProjectileSource shooter;
		Entity entity_arrow = evt.getDamager();
		Player p, p1;
		Arrow arrow;
		GamePlayer player, player1;

		if(!(entity instanceof Player))
			return;
		p = (Player)entity;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		if(evt.getCause() != EntityDamageEvent.DamageCause.PROJECTILE
				|| entity_arrow.getType() != EntityType.ARROW)
			return;

		if(!(entity_arrow instanceof Arrow))
			return;
		arrow = (Arrow)entity_arrow;

		shooter = arrow.getShooter();
		if(!(shooter instanceof Player))
			return;
		p1 = (Player)shooter;

		player1 = this.plugin.getPlayerManager().getPlayer(p1);
		if(player1 == null)
			return;

		if(player.getStatus() != GamePlayerStatus.PLAYING
				|| player1.getStatus() != GamePlayerStatus.PLAYING)
			return;

		player1.sendMessage("§7Vie de §c" + player.getName() + "§7 : §c" + (int)(p.getHealth() - evt.getFinalDamage()) + "❤");
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onEatGoldenApple(PlayerItemConsumeEvent evt) {
		Player p = evt.getPlayer();
		ItemStack item = evt.getItem();
		GamePlayer player;
		Game game;
		PlayerInventory inventory;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		if(player.getStatus() != GamePlayerStatus.PLAYING)
			return;

		game = player.getGame();
		if(game == null)
			return;

		if(item.getType() != Material.GOLDEN_APPLE)
			return;

		if(item.getDurability() != 0)
			return;

		if(!"§6Golden head".equals(item.getItemMeta().getDisplayName()))
			return;

		inventory = p.getInventory();
		item = inventory.getItem(inventory.getHeldItemSlot());
		item.setAmount(item.getAmount() - 1);
		if(item.getAmount() <= 0)
			item.setType(Material.AIR);
		inventory.setItem(inventory.getHeldItemSlot(), item);
		p.updateInventory();

		p.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 3 * 60 * Utils.getTicksPerSec(), 0), true);
		p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 10 * Utils.getTicksPerSec(), 1), true);
		evt.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onBlockExpands(BlockFromToEvent evt) {
		try {
			MetadataValue metadata = evt.getBlock().getMetadata("FunPractice.WasPutByPlayer").stream()
					.filter((value) -> value.getOwningPlugin() == this.plugin)
					.findAny()
					.get();
			if(metadata.asBoolean()) {
				evt.getToBlock().setMetadata("FunPractice.WasPutByPlayer", new FixedMetadataValue(this.plugin, true));
			}
		} catch(NoSuchElementException ex) {}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onFoodLevelChange(FoodLevelChangeEvent evt) {
		evt.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onEnderPearlTeleport(PlayerTeleportEvent evt) {
		Player p = evt.getPlayer();
		GamePlayer player;

		if(evt.getCause() != PlayerTeleportEvent.TeleportCause.ENDER_PEARL)
			return;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		if(player.getStatus() == GamePlayerStatus.PLAYING)
			return;

		evt.setCancelled(true);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerRealDie(PlayerDeathEvent evt) {
		Player p = evt.getEntity();
		GamePlayer player;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		Bukkit.getScheduler().runTask(plugin, () -> {
			onPlayerQuit(new PlayerQuitEvent(p, ""));
			onPlayerJoin(new PlayerJoinEvent(p, ""));
		});
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerFall(PlayerMoveEvent evt) {
		Player p = evt.getPlayer();
		GamePlayer player;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		if(evt.getTo().getY() >= this.plugin.getConfigManager().getPlayerMinY())
			return;

		switch (player.getStatus()) {
			case PLAYING:
				this.plugin.getGameManager().removePlayer(player);
				break;

			case EDITING_KIT:
				player.teleport(this.plugin.getConfigManager().getKitEditZoneSpawn());
				break;

			default:
				player.teleportToSpawn();
				break;
		}
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayingPlayerLeavesArena(PlayerMoveEvent evt) {
		Player p = evt.getPlayer();
		GamePlayer player;
		Location loc;
		RectMeta coords;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		if(player.getStatus() != GamePlayerStatus.PLAYING)
			return;

		loc = evt.getTo();
		coords = player.getGame().getArena().getCoords();
		if(loc.getX() >= coords.getMinX()
				&& loc.getX() <= coords.getMaxX()
				&& loc.getZ() >= coords.getMinZ()
				&& loc.getZ() <= coords.getMaxZ())
			return;

		player.getGame().removePlayer(player);
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerCraft(CraftItemEvent evt) {
		HumanEntity human = evt.getWhoClicked();
		Player p;
		GamePlayer player;

		if(!(human instanceof Player))
			return;
		p = (Player)human;

		player = this.plugin.getPlayerManager().getPlayer(p);
		if(player == null)
			return;

		if(player.getStatus() != GamePlayerStatus.PLAYING)
			return;

		if(evt.getRecipe().getResult().getType() != Material.BOAT
				&& evt.getRecipe().getResult().getType() != Material.MINECART)
			return;

		evt.setCancelled(true);
	}

}
