/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 *
 * @author guillaume
 */
public class Game {

	private final FunPractice plugin;
	private final GameType type;
	private GamePlayer players[];
	private final Map<UUID, Inventory> savedInventories;
	private GameStatus status;
	private ArenaMeta arena;

	public Game(FunPractice plugin, GameType type) {
		this(plugin, type, new GamePlayer[0]);
	}

	private Game(FunPractice plugin, GameType type, GamePlayer... p) {
		this.plugin = plugin;
		this.type = type;
		this.players = Arrays.copyOf(p, 2);
		this.savedInventories = new HashMap<>();
		this.status = GameStatus.WAITING;
		this.arena = null;
	}

	public GameType getType() {
		return this.type;
	}

	public GamePlayer[] getPlayers() {
		return this.players;
	}

	public GameStatus getStatus() {
		return this.status;
	}

	public ArenaMeta getArena() {
		return this.arena;
	}

	public int getMaxPlayers() {
		return 2;
	}

	public int countPlayers() {
		return (int)Utils.arrayStreamNonNull(this.players).count();
	}

	public void addPlayer(GamePlayer player) {
		if (isFull())
			return;

		if (this.status != GameStatus.WAITING)
			return;

		if (this.getType().getRanked())
		{
			if (!player.isRankedAllowed())
				return;

			player.incrementRankedCount();
		}

		this.players[Utils.getFirstNullArrayIndice(this.players)] = player;
		player.setGame(this);
		player.setStatus(GamePlayerStatus.WAITING_GAME);
		player.openInventory(new GameLeaveInventory(this.plugin, player));

		sendAllMessage("Nouveau joueur : " + player.getName());

		if(!isFull()) {
			int remaining = getMaxPlayers() - countPlayers();

			if(remaining == 1)
				sendAllMessage("En attente d'un joueur...");
			else
				sendAllMessage("En attente de " + remaining + " joueurs...");
			return;
		}

		sendAllMessage(countPlayers() + "/" + getMaxPlayers() + " joueurs, la partie commencera dans quelques instants...");

		this.status = GameStatus.LAUNCHING;
		Utils.countDown(this.plugin, 3, 0, () -> {
			if(countPlayers() != getMaxPlayers()) {
				sendAllMessage("Lancement annulé.");
				this.status = GameStatus.WAITING;
				return;
			}
			this.status = GameStatus.PRELAUNCHED;

			for(GamePlayer p_ : this.players) {
				p_.closeInventory();
			}
			sendAllMessage("Lancement de " + this.type + " #" + this.plugin.getGameManager().getGameID(this) + "...");
			sendAllMessage("En attente d'une arène libre...");
			prepare_launch(true);
		}, (count) -> {
			String[] color = { "§c", "§e", "§a" };

			sendAllMessage(color[(color.length - count) % color.length] + count + "...".substring(0, 1 + ((count - 1) % color.length)));
			return null;
		});
	}

	private void prepare_launch(boolean first_call) {
		final Runnable task = () -> {
			this.arena = this.plugin.getConfigManager().attachGame(this);
			if(this.arena == null) {
				prepare_launch(false);
				return;
			}
			launch();
		};
		if(first_call) {
			task.run();
			return;
		}
		Bukkit.getScheduler().runTaskLater(this.plugin, task, 1 * Utils.getTicksPerSec());
	}

	public void removePlayer(GamePlayer p) {
		if(p.getGame() != this)
			return;

		if(getStatus() == GameStatus.PRELAUNCHED) {
			p.sendMessage("Vous êtes mort !");
			sendAllMessageExcept(p, p.getName() + " est mort !");
			this.status = (p == this.players[0]) ? GameStatus.ABORTED_0 : GameStatus.ABORTED_1;
		} else if(getStatus() == GameStatus.LAUNCHED) {
			p.sendMessage("Vous êtes mort !");
			sendAllMessageExcept(p, p.getName() + " est mort !");
			complete(p);
		} else if(getStatus() != GameStatus.COMPLETED
				&& getStatus() != GameStatus.ABORTED_0
				&& getStatus() != GameStatus.ABORTED_1) {
			p.sendMessage("Vous avez quitté le jeu " + getType() + " !");
			sendAllMessageExcept(p, p.getName() + " a quitté le jeu !");
			for(int i = 0; i < this.players.length; i++)
				if(this.players[i] == p)
					this.players[i] = null;
			p.setStatus(GamePlayerStatus.IDLE);
			p.resetState();
		}
	}

	public boolean containsPlayer(GamePlayer player) {
		return Utils.arrayStreamNonNull(this.players).anyMatch((p) -> (p == player));
	}

	public boolean isFull() {
		return countPlayers() >= getMaxPlayers();
	}

	public void sendAllMessage(String message) {
		Utils.arrayStreamNonNull(this.players).forEach((player) -> {
			player.sendMessage(message);
		});
	}

	public void sendAllMessageExcept(GamePlayer player, String message) {
		Utils.arrayStreamNonNull(this.players).forEach((p) -> {
			if(p != player)
				p.sendMessage(message);
		});
	}

	public void launch() {
		Utils.arrayStreamNonNull(this.players).forEach((player) -> {
			player.setStatus(GamePlayerStatus.ABOUT_TO_PLAY);
		});

		sendAllMessage("Préparation de l'arène...");

		final Runnable next_task = () -> {
			this.plugin.getWorld().getEntities().stream().forEach((ent) -> {
				Location loc = ent.getLocation();
				RectMeta rect = this.arena.getCoords();

				if (loc.getX() >= rect.getMinX()
						&& loc.getZ() >= rect.getMinZ()
						&& loc.getX() <= rect.getMaxX()
						&& loc.getZ() <= rect.getMaxZ())
				{
					if (ent instanceof Player)
					{
						Player p = (Player)ent;

						/* Don't kill detached players */
						if (this.plugin.getPlayerManager().getPlayer(p) != null)
							Utils.sendEntityDamageEvent(this.plugin, p, p.getMaxHealth());
					}
					else
						ent.remove();
				}
			});

			String str = this.type + " #" + this.plugin.getGameManager().getGameID(this) + " lancé dans l'arène " + this.arena.getName();
			sendAllMessage(str);

			str = this.type.getRanked() ? this.players[0].getRankAndName(this.type) : this.players[0].getName();
			str += " VS ";
			str += this.type.getRanked() ? this.players[1].getRankAndName(this.type) : this.players[1].getName();
			sendAllMessage(str);

			List<SpawnMeta> spawns = Arrays.asList(this.arena.getSpawns());
			Collections.shuffle(spawns, new Random());
			for(int i = 0; i < this.players.length; i++) {
				GamePlayer player = this.players[i];
				SpawnMeta spawn = spawns.get(i % spawns.size());
				Player p = player.getPlayer();
				PlayerInventory invt;
				KitMeta kit = player.getKit(this.type);

				player.removeAllPotionEffects();
				player.clearInventory();
				player.setFlyEnabled(false);
				invt = p.getInventory();
				invt.setContents(kit.getItems());
				invt.setHelmet(kit.getHelmet());
				invt.setChestplate(kit.getChestplate());
				invt.setLeggings(kit.getLeggings());
				invt.setBoots(kit.getBoots());
				p.updateInventory();
				p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 2 * Utils.getTicksPerSec(), 10), true);
				player.setStatus(GamePlayerStatus.PLAYING);
				player.teleport(spawn);
			}

			if(this.status == GameStatus.ABORTED_0) {
				complete(this.players[0]);
				return;
			}

			if(this.status == GameStatus.ABORTED_1) {
				complete(this.players[1]);
				return;
			}
			this.status = GameStatus.LAUNCHED;
		};

		if(this.arena.getNeedsFullCleanup()) {
			Bukkit.getScheduler().runTaskAsynchronously(this.plugin, () -> {
				Utils.copyBlocksBetweenWorlds(this.plugin, this.plugin.getWorld(), this.plugin.getTemplateWorld(), this.arena.getCoords(), next_task);
			});
		} else
			next_task.run();
	}

	public void complete(GamePlayer looser) {
		GamePlayer winner;
		int id = this.plugin.getGameManager().getGameID(this);
		int ELO_delta = 0;

		if(this.status != GameStatus.ABORTED_0 && this.status != GameStatus.ABORTED_1 && this.status != GameStatus.LAUNCHED)
			return;

		this.status = GameStatus.COMPLETED;
		this.sendAllMessage(this.type + " #" + id + " terminé !");

		winner = this.players[0];
		if(winner == looser)
			winner = this.players[1];

		if(this.type.getRanked())
			ELO_delta = GamePlayer.updateELO(winner, looser);

		for(GamePlayer p : this.players) {
			TextComponent t;

			p.setStatus(GamePlayerStatus.IDLE);

			this.savedInventories.put(p.getUniqueId(), GameEndInventory.inventoryFromPlayerInventory(this.plugin,
					p.getPlayer().getInventory(),
					"Inventaire de " + p.getName(),
					p == looser ? 0 : (int)p.getPlayer().getHealth(),
					p.getPlayer().getFoodLevel(),
					p.getPlayer().getActivePotionEffects()));

			t = new TextComponent("§7Gagnant : " + (this.type.getRanked() ? Utils.ELOToRank(winner.getELO(this.type)) + " " : "") + "§a" + winner.getName()
					+ (this.type.getRanked() ? " §e" + winner.getELO(this.type) + " §7(§2+" + ELO_delta + "§7) §6ELO" : ""));
			t.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/showgameinventory " + id + " " + winner.getUniqueId().toString()));
			t.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[] { new TextComponent("Inventaire de " + winner.getName()) }));
			p.sendMessage(t);

			t = new TextComponent("§7Perdant : " + (this.type.getRanked() ? Utils.ELOToRank(looser.getELO(this.type)) + " " : "") + "§c" + looser.getName()
					+ (this.type.getRanked() ? " §e" + looser.getELO(this.type) + " §7(§4-" + ELO_delta + "§7) §6ELO" : ""));
			t.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/showgameinventory " + id + " " + looser.getUniqueId().toString()));
			t.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new BaseComponent[] { new TextComponent("Inventaire de " + looser.getName()) }));
			p.sendMessage(t);

			p.resetState();
			p.teleportToSpawn();
		}

		this.plugin.getConfigManager().detachGame(this);
	}

	public void showSavedInventory(GamePlayer player, UUID show_player) {
		Inventory invt = this.savedInventories.get(show_player);

		if(invt == null)
			return;
		player.openInventory(new GameEndInventory(this.plugin, player, invt));
	}

}
