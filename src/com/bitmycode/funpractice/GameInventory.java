/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author guillaume
 */
public class GameInventory {

	protected final FunPractice plugin;
	private final GameInventoryType type;
	private final boolean replacePlayerInventory;
	private final GamePlayer player;
	private final Inventory base_inventory;
	private Inventory inventory;
	private Runnable onCloseTask;
	private boolean anticipatedClose;

	public GameInventory(FunPractice plugin, GameInventoryType type, boolean replace_player_inventory, GamePlayer player, int nb_items) {
		this(plugin, type, replace_player_inventory, player, nb_items, "Inventaire");
	}

	public GameInventory(FunPractice plugin, GameInventoryType type, boolean replace_player_inventory, GamePlayer player, int nb_items, String title) {
		this.plugin = plugin;
		this.type = type;
		this.replacePlayerInventory = replace_player_inventory;
		this.player = player;
		this.base_inventory = this.plugin.getServer().createInventory(player.getPlayer(), nb_items, title);
		this.inventory = null;
		this.onCloseTask = null;
		this.anticipatedClose = false;
	}

	public GameInventoryType getType() {
		return this.type;
	}

	public GamePlayer getPlayer() {
		return this.player;
	}

	public boolean getReplacePlayerInventory() {
		return this.replacePlayerInventory;
	}

	public boolean getAnticipatedClose() {
		return this.anticipatedClose;
	}

	protected void addItem(ItemStack... items) {
		this.base_inventory.addItem(items);
	}

	protected void setContents(ItemStack[] items) {
		this.base_inventory.setContents(items);
	}

	public void open() {
		Player p = this.player.getPlayer();

		if(this.replacePlayerInventory) {
			p.getInventory().setContents(this.base_inventory.getContents());
			p.updateInventory();
			this.inventory = this.base_inventory;
		} else
			this.inventory = p.openInventory(this.base_inventory).getTopInventory();
		this.anticipatedClose = false;
	}

	public void close() {
		Player p = this.player.getPlayer();

		if(this.inventory == null)
			return;

		if(this.replacePlayerInventory) {
			p.getInventory().clear();
			p.updateInventory();
		}
		this.inventory = null;

		if(this.onCloseTask != null)
			this.onCloseTask.run();
	}

	public boolean anticipatedClose() {
		this.anticipatedClose = true;
		if(this.replacePlayerInventory)
			return true;

		this.player.getPlayer().closeInventory();
		return false;
	}

	public void setOnClose(Runnable task) {
		this.onCloseTask = task;
	}

	public boolean belongsTo(Inventory invt) {
		return invt == this.inventory;
	}

}
