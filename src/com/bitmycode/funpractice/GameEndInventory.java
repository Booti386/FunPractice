/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;

/**
 *
 * @author guillaume
 */
public class GameEndInventory extends GameInventory {

	public GameEndInventory(FunPractice plugin, GamePlayer player, Inventory inventory) {
		super(plugin, GameInventoryType.GAME_END, false, player, 6 * 9, inventory.getTitle());

		super.setContents(inventory.getContents());
	}

	public static Inventory inventoryFromPlayerInventory(FunPractice plugin, PlayerInventory player_invt, String title, int health, int food_lvl, Collection<PotionEffect> potion_effects) {
		final ItemStack[] player_invt_contents = player_invt.getContents();
		Inventory invt = plugin.getServer().createInventory(null, 6 * 9, title);
		ItemStack[] contents = new ItemStack[6 * 9];
		List<String> potion_effects_str = new LinkedList<>();
		int i = 0;
		ItemStack item;
		ItemMeta meta;

		/* 1st line */
		contents[i++] = player_invt.getHelmet();
		contents[i++] = player_invt.getChestplate();
		contents[i++] = player_invt.getLeggings();
		contents[i++] = player_invt.getBoots();
		while(i < 9)
			contents[i++] = new ItemStack(Material.AIR);

		/* 2nd-5th lines */
		System.arraycopy(player_invt_contents, 0, contents, 1 * 9, 4 * 9);
		i += 4 * 9;

		/* 6th line */
		while(i < 5 * 9 + 3)
			contents[i++] = new ItemStack(Material.AIR);

		item = new ItemStack(Material.SKULL_ITEM);
		if(health != 0)
			item.setDurability((short)3); // Player head
		else
			item.setDurability((short)1); // Wither head

		meta = item.getItemMeta();
		if(health != 0) {
			meta.setDisplayName("§cVie");
			meta.setLore(Arrays.asList(new String[] { "§7" + health + "❤" }));
		} else
			meta.setDisplayName("§cJoueur mort");
		item.setItemMeta(meta);
		contents[i++] = item;

		item = new ItemStack(Material.COOKED_BEEF);
		meta = item.getItemMeta();
		meta.setDisplayName("§cNourriture");
		meta.setLore(Arrays.asList(new String[] { "§7" + food_lvl }));
		item.setItemMeta(meta);
		contents[i++] = item;

		item = new ItemStack(Material.BREWING_STAND_ITEM);
		meta = item.getItemMeta();
		meta.setDisplayName("§cEffets de potions");
		potion_effects.stream().forEach((effect) -> {
			int time = effect.getDuration() / Utils.getTicksPerSec();
			int time_min = time / 60;
			int time_sec = time - (time_min * 60);
			String effect_name = effect.getType().getName().toLowerCase();
			String lvl= Utils.toRomanNumber(effect.getAmplifier() + 1);

			effect_name = effect_name.substring(0, 1).toUpperCase() + effect_name.substring(1);
			effect_name = effect_name.replace('_', ' ');
			potion_effects_str.add("§7" + effect_name + " " + lvl + " : " + time_min + "m" + time_sec);
		});
		meta.setLore(potion_effects_str);
		item.setItemMeta(meta);
		contents[i++] = item;

		while(i < 6 * 9)
			contents[i++] = new ItemStack(Material.AIR);

		invt.setContents(contents);
		return invt;
	}

	public static GameEndInventory fromInventory(GameInventory invt) {
		if(invt instanceof GameEndInventory)
			return (GameEndInventory)invt;
		return null;
	}

}
