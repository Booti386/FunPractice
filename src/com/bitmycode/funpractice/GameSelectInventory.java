/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import java.util.Arrays;
import org.bukkit.Material;

import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionType;

/**
 *
 * @author guillaume
 */
public class GameSelectInventory extends GameSelectInventoryHelper {

	private final boolean isRanked;
	private final GamePlayer player1;

	public GameSelectInventory(FunPractice plugin, GamePlayer player, boolean is_ranked) {
		this(plugin, player, null, is_ranked);
	}

	public GameSelectInventory(FunPractice plugin, GamePlayer player, GamePlayer player1, boolean is_ranked) {
		super(plugin, GameInventoryType.GAME_SELECT, false, player, 9, "Sélection de jeu");

		ItemStack[] contents = new ItemStack[9];
		int i = 0;
		ItemStack item;

		this.isRanked = is_ranked;
		this.player1 = player1;

		/* 1st line */

		/* Splash Potion - Instant Heal II */
		item = Utils.createSplashPotionItem(PotionType.INSTANT_HEAL, true);
		itemStackSetMeta(item, this.isRanked ? GameType.RANKED_NO_DEBUFF : GameType.NO_DEBUFF);
		contents[i++] = item;

		contents[i++] = newItemStackFromGameType(Material.LAVA_BUCKET, this.isRanked ? GameType.RANKED_BUILD_UHC : GameType.BUILD_UHC);

		/* Potion - Water */
		item = Utils.createPotionItem(PotionType.WATER, false);
		itemStackSetMeta(item, this.isRanked ? GameType.RANKED_BUILD_POT : GameType.BUILD_POT);
		contents[i++] = item;

		/* Enchanted Golden Apple */
		item = newItemStackFromGameType(Material.GOLDEN_APPLE, this.isRanked ? GameType.RANKED_G_APPLE : GameType.G_APPLE);
		item.setDurability((short)1);
		contents[i++] = item;

		contents[i++] = newItemStackFromGameType(Material.BOW, this.isRanked ? GameType.RANKED_ARCHER : GameType.ARCHER);
		contents[i++] = newItemStackFromGameType(Material.IRON_CHESTPLATE, this.isRanked ? GameType.RANKED_IRON : GameType.IRON);

		if(!is_ranked) {
			contents[i++] = newItemStackFromGameType(Material.ENCHANTMENT_TABLE, GameType.HC_NO_ENCHANTS);
			contents[i++] = newItemStackFromGameType(Material.FISHING_ROD, GameType.SG);
		}

		while(i < 1 * 9)
			contents[i++] = new ItemStack(Material.AIR);

		super.setContents(contents);
	}

	private ItemStack itemStackSetMeta(ItemStack item, GameType game_type) {
		GameManager game_mgr = this.plugin.getGameManager();
		long nb_waiting = Arrays.asList(game_mgr.getWaitingOrLaunchingGames(game_type)).stream().mapToLong((game) -> game.countPlayers()).sum();
		long nb_fighting = Arrays.asList(game_mgr.getLaunchedGames(game_type)).stream().mapToLong((game) -> game.countPlayers()).sum();

		item.setAmount((int)nb_fighting);
		itemStackSetName(item, game_type.toString());
		itemStackSetLore(item, new String[] {
			"§eEn attente : " + nb_waiting,
			"§eEn combat : " + nb_fighting
		});
		return item;
	}

	private ItemStack newItemStackFromGameType(Material type, GameType game_type) {
		ItemStack item = new ItemStack(type, 1);
		itemStackSetMeta(item, game_type);
		return item;
	}

	public boolean isRanked() {
		return this.isRanked;
	}

	public GamePlayer getPlayer1() {
		return this.player1;
	}

	public GameType getGameType(int slot) {
		if(this.isRanked())
			return getRankedGameType(slot);
		return getUnrankedGameType(slot);
	}

	public static GameSelectInventory fromInventory(GameInventory invt) {
		if(invt instanceof GameSelectInventory)
			return (GameSelectInventory)invt;
		return null;
	}

}
