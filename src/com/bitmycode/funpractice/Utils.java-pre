/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import com.google.common.collect.ImmutableMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.function.Function;
import java.util.stream.Stream;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionBrewer;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionType;

//#if SPIGOT_API_VERSION >= MAKEVER(1, 8, 0)
import org.bukkit.inventory.ItemFlag;
//#endif

//#if SPIGOT_API_VERSION >= MAKEVER(1, 9, 0)
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.Potion;
//#else
import org.bukkit.potion.Potion;
//#endif

/**
 *
 * @author guillaume
 */
public class Utils {

	public static <T extends Comparable<T>> T clamp(T val, T min, T max) {
		if(val.compareTo(min) < 0)
			return min;
		if(val.compareTo(max) > 0)
			return max;
		return val;
	}

	public static <T> int getFirstNullArrayIndice(T[] arr) {
		for(int i = 0; i < arr.length; i++)
			if(arr[i] == null)
				return i;
		return -1;
	}

	public static <T> Stream<T> arrayStreamNonNull(T[] arr) {
		return Arrays.stream(arr).filter((elem) -> elem != null);
	}

	/**
	 * Get the number of server ticks in 1000 ms
	 *
	 * @return The number of ticks
	 */
	public static int getTicksPerSec() {
		return 1000 / 50;
	}

	public static String toRomanNumber(int nb) {
		switch(nb) {
			case 1: return "I";
			case 2: return "II";
			case 3: return "III";
			case 4: return "IV";
			case 5: return "V";
			case 6: return "VI";
			case 7: return "VII";
			case 8: return "VIII";
			case 9: return "IX";
			case 10: return "X";
			default: return "" + nb;
		}
	}

	public static String ELOToRank(int ELO) {
		if(ELO < 690)
			return "§fAir§f";
		else if(ELO < 720)
			return "§cBois V§f";
		else if(ELO < 740)
			return "§cBois IV§f";
		else if(ELO < 760)
			return "§cBois III§f";
		else if(ELO < 780)
			return "§cBois II§f";
		else if(ELO < 800)
			return "§c§lBois I§r§f";
		else if(ELO < 840)
			return "§8Pierre III§f";
		else if(ELO < 860)
			return "§8Pierre II§f";
		else if(ELO < 940)
			return "§8§lPierre I§r§f";
		else if(ELO < 980)
			return "§7Fer V§f";
		else if(ELO < 1040)
			return "§7Fer IV§f";
		else if(ELO < 1070)
			return "§7Fer III§f";
		else if(ELO < 1100)
			return "§7Fer II§f";
		else if(ELO < 1150)
			return "§7§lFer I§r§f";
		else if(ELO < 1220)
			return "§6Or V§f";
		else if(ELO < 1280)
			return "§6Or IV§f";
		else if(ELO < 1350)
			return "§6Or III§f";
		else if(ELO < 1450)
			return "§6Or II§f";
		else if(ELO < 1570)
			return "§6§lOr I§r§f";
		else if(ELO < 1620)
			return "§bDiamant IV§f";
		else if(ELO < 1680)
			return "§bDiamant III§f";
		else if(ELO < 1760)
			return "§bDiamant II§f";
		else if(ELO < 1900)
			return "§b§lDiamant I§r§f";
		else if(ELO < 1940)
			return "§aÉmeraude II§f";
		else if(ELO < 2000)
			return "§a§lÉmeraude I§r§f";
		else
			return "§3Platine§f";
	}

	public static Date truncateDateToDay(Date date)
	{
		Calendar c = new Calendar.Builder().setInstant(date).build();

		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);

		return c.getTime();
	}

	public static void countDown(Plugin plugin, int start, int end, Runnable task, Function<Integer, Void> msg_func) {
		if(start < end) {
			task.run();
			return;
		}

		Bukkit.getScheduler().runTaskLater(plugin, () -> {
			if(start != end)
				msg_func.apply(start);
			countDown(plugin, start - 1, end, task, msg_func);
		}, Utils.getTicksPerSec());
	}

	public static void copyBlocksBetweenWorlds(FunPractice plugin, World dst, World src, RectMeta rect, Runnable task) {
		final int min_y = plugin.getConfigManager().getArenaMinY();
		final int max_y = plugin.getConfigManager().getArenaMaxY();
		final int max_layer_updates_per_tick = plugin.getConfigManager().getMaxLayerUpdatesPerTick();
		final List<Semaphore> semaphores = new ArrayList<>();

		if(rect.getMinX() > rect.getMaxX()
				|| rect.getMinZ() > rect.getMaxZ()
				|| max_y < 0)
			return;

		for(int y = min_y; y <= max_y; y++) {
			final int yy = y;
			final Semaphore semaphore = new Semaphore(0);

			Bukkit.getScheduler().runTaskLater(plugin, () -> {
				for(int x = rect.getMinX(); x <= rect.getMaxX(); x++) {
					for(int z = rect.getMinZ(); z <= rect.getMaxZ(); z++) {
						Block dst_block = dst.getBlockAt(x, yy, z);
						Block src_block = src.getBlockAt(x, yy, z);
						BlockState dst_block_state = dst_block.getState();
						BlockState src_block_state = src_block.getState();

						dst_block_state.setType(src_block_state.getType());
						dst_block_state.setData(src_block_state.getData());
						dst_block_state.update(true);
					}
				}

				semaphore.release();
			}, y / max_layer_updates_per_tick);
			semaphores.add(semaphore);
		}

		semaphores.stream().forEach((sync) -> {
			boolean was_interrupted = true;

			while(was_interrupted) {
				was_interrupted = false;

				try {
					sync.acquire();
				} catch (InterruptedException ex) {
					was_interrupted = true;
				}
			}
		});
		Bukkit.getScheduler().runTask(plugin, task);
	}

	public static ItemStack getPlayerItemInMainHand(Player player) {
		ItemStack item;

//#if SPIGOT_API_VERSION >= MAKEVER(1, 9, 0)
		item = player.getInventory().getItemInMainHand();
//#else
		item = player.getItemInHand();
//#endif

		return item;
	}

	public static ItemMeta itemMetaHideAttrsAndPotionEffects(ItemMeta meta) {
//#if SPIGOT_API_VERSION >= MAKEVER(1, 8, 0)
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
//#endif

		return meta;
	}

	public static ItemStack createPotionItem(PotionType type, boolean upgraded) {
		ItemStack item;

//#if SPIGOT_API_VERSION >= MAKEVER(1, 9, 0)
		PotionMeta potion_meta;

		item = new ItemStack(Material.POTION, 1);
		potion_meta = (PotionMeta)item.getItemMeta();
		potion_meta.setBasePotionData(new PotionData(type, false, upgraded));
		item.setItemMeta(potion_meta);
//#else
		Potion potion = new Potion(type);

		if(type.getMaxLevel() > 0)
			potion.setLevel(upgraded ? 2 : 1);
		item = potion.toItemStack(1);
//#endif

		return item;
	}

	public static ItemStack createSplashPotionItem(PotionType type, boolean upgraded) {
		ItemStack item;

//#if SPIGOT_API_VERSION >= MAKEVER(1, 9, 0)
		PotionMeta potion_meta;

		item = new ItemStack(Material.SPLASH_POTION, 1);
		potion_meta = (PotionMeta)item.getItemMeta();
		potion_meta.setBasePotionData(new PotionData(type, false, upgraded));
		item.setItemMeta(potion_meta);
//#else
		Potion potion = new Potion(type).splash();

		if(type.getMaxLevel() > 0)
			potion.setLevel(upgraded ? 2 : 1);
		item = potion.toItemStack(1);
//#endif

		return item;
	}

	public static boolean itemIsPotion(ItemStack item) {
//#if SPIGOT_API_VERSION >= MAKEVER(1, 9, 0)
		return item.getType() == Material.POTION;
//#else
		if(item.getType() == Material.POTION) {
			/* Water is never splash */
			if(item.getDurability() == 0)
				return true;
			return !Potion.fromItemStack(item).isSplash();
		}
		return false;
//#endif
	}

	public static boolean itemIsSplashPotion(ItemStack item) {
//#if SPIGOT_API_VERSION >= MAKEVER(1, 9, 0)
		return item.getType() == Material.SPLASH_POTION;
//#else
		if(item.getType() == Material.POTION) {
			/* Water is never splash */
			if(item.getDurability() == 0)
				return false;
			return Potion.fromItemStack(item).isSplash();
		}
		return false;
//#endif
	}

	public static PotionBrewer getPotionBrewer() {
		return Potion.getBrewer();
	}

	public static Collection<PotionEffect> getPotionEffectsFromItemStack(ItemStack item) {
		Collection<PotionEffect> effects = new ArrayList<>();

		if(item.getType() != Material.POTION)
			return effects;

//#if SPIGOT_API_VERSION >= MAKEVER(1, 9, 0)
		ItemMeta item_meta;
		PotionMeta meta;
		PotionData potion_data;

		item_meta = item.getItemMeta();
		if(!(item_meta instanceof PotionMeta))
			return effects;
		meta = (PotionMeta)item_meta;

		potion_data = meta.getBasePotionData();
		effects = Utils.getPotionBrewer().getEffects(potion_data.getType(), potion_data.isUpgraded(), potion_data.isExtended());
//#else
		effects = Potion.fromItemStack(item).getEffects();
//#endif

		return effects;
	}

	public static void sendEntityDamageEvent(FunPractice plugin, Entity ent, double damage) {
		final Map<EntityDamageEvent.DamageModifier, Double> damage_map = new EnumMap<>(ImmutableMap.of(EntityDamageEvent.DamageModifier.BASE, damage));
		final Map<EntityDamageEvent.DamageModifier, com.google.common.base.Function<? super Double, Double>> damage_func_map = new EnumMap<>(ImmutableMap.of(EntityDamageEvent.DamageModifier.BASE, (a) -> { return 0.0; }));

		plugin.getServer().getPluginManager().callEvent(new EntityDamageEvent(ent, EntityDamageEvent.DamageCause.CUSTOM, damage_map, damage_func_map));
	}

	/**
	 * Sets the generator settings of the world that will be created or loaded
	 * Supported only on >= 1.8 (ignored on <= 1.7)
	 *
	 * @param world_creator The world creator
	 * @param settings  The settings that should be used by the generator
	 * @return The world creator, for chaining
	 */
	public static WorldCreator setGeneratorSettings(WorldCreator world_creator, String settings) {
//#if SPIGOT_API_VERSION >= MAKEVER(1, 8, 0)
		return world_creator.generatorSettings(settings);
//#else
		return world_creator;
//#endif
	}

}
