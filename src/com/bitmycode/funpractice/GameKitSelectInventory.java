/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionType;

/**
 *
 * @author guillaume
 */
public class GameKitSelectInventory extends GameSelectInventoryHelper {

	public GameKitSelectInventory(FunPractice plugin, GamePlayer player) {
		super(plugin, GameInventoryType.GAME_SELECT, false, player, 3 * 9, "Sélection de kit");

		ItemStack[] contents = new ItemStack[3 * 9];
		int i = 0;
		ItemStack item;

		/* 1st line */

		/* Splash Potion - Instant Heal II */
		item = Utils.createSplashPotionItem(PotionType.INSTANT_HEAL, true);
		itemStackSetMeta(item, GameType.NO_DEBUFF);
		contents[i++] = item;

		contents[i++] = newItemStackFromGameType(Material.LAVA_BUCKET, GameType.BUILD_UHC);

		/* Potion - Water */
		item = Utils.createPotionItem(PotionType.WATER, false);
		itemStackSetMeta(item, GameType.BUILD_POT);
		contents[i++] = item;

		/* Enchanted Golden Apple */
		item = newItemStackFromGameType(Material.GOLDEN_APPLE, GameType.G_APPLE);
		item.setDurability((short)1);
		contents[i++] = item;

		contents[i++] = newItemStackFromGameType(Material.BOW, GameType.ARCHER);
		contents[i++] = newItemStackFromGameType(Material.IRON_CHESTPLATE, GameType.IRON);

		contents[i++] = newItemStackFromGameType(Material.ENCHANTMENT_TABLE, GameType.HC_NO_ENCHANTS);
		contents[i++] = newItemStackFromGameType(Material.FISHING_ROD, GameType.SG);

		while(i < 1 * 9)
			contents[i++] = new ItemStack(Material.AIR);

		/* 2nd line */

		while(i < 2 * 9)
			contents[i++] = new ItemStack(Material.THIN_GLASS);

		/* 3rd line */

		if(this.plugin.getConfigManager().getRankedEnabled()) {
			/* Splash Potion - Instant Heal II */
			item = Utils.createSplashPotionItem(PotionType.INSTANT_HEAL, true);
			itemStackSetMeta(item, GameType.RANKED_NO_DEBUFF);
			contents[i++] = item;

			contents[i++] = newItemStackFromGameType(Material.LAVA_BUCKET, GameType.RANKED_BUILD_UHC);

			/* Potion - Water */
			item = Utils.createPotionItem(PotionType.WATER, false);
			itemStackSetMeta(item, GameType.RANKED_BUILD_POT);
			contents[i++] = item;

			/* Enchanted Golden Apple */
			item = newItemStackFromGameType(Material.GOLDEN_APPLE, GameType.RANKED_G_APPLE);
			item.setDurability((short)1);
			contents[i++] = item;

			contents[i++] = newItemStackFromGameType(Material.BOW, GameType.RANKED_ARCHER);
			contents[i++] = newItemStackFromGameType(Material.IRON_CHESTPLATE, GameType.RANKED_IRON);
		}

		while(i < 3 * 9)
			contents[i++] = new ItemStack(Material.AIR);

		super.setContents(contents);
	}

	private ItemStack itemStackSetMeta(ItemStack item, GameType game_type) {
		itemStackSetName(item, game_type.toString() + (game_type.getRanked() ? " (ranked)" : " (unranked)"));
		return item;
	}

	private ItemStack newItemStackFromGameType(Material type, GameType game_type) {
		ItemStack item = new ItemStack(type, 1);
		itemStackSetMeta(item, game_type);
		return item;
	}

	public GameType getGameType(int slot) {
		if(slot < 1 * 9)
			return getUnrankedGameType(slot - 0);
		else if(slot >= 2 * 9)
			return getRankedGameType(slot - 2 * 9);
		return null;
	}

	public static GameKitSelectInventory fromInventory(GameInventory invt) {
		if(invt instanceof GameKitSelectInventory)
			return (GameKitSelectInventory)invt;
		return null;
	}

}
