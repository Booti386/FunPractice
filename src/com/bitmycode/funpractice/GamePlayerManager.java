/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import java.util.HashMap;
import java.util.Map;
import org.bukkit.entity.Player;

/**
 *
 * @author guillaume
 */
public class GamePlayerManager {
	private final FunPractice plugin;
	private final Map<Player, GamePlayer> players;

	public GamePlayerManager(FunPractice plugin) {
		this.plugin = plugin;
		this.players = new HashMap<>();
	}

	public GamePlayer getPlayer(Player player) {
		return this.players.get(player);
	}

	public int getPlayerCount() {
		return this.players.size();
	}

	public GamePlayer addPlayer(Player player) {
		GamePlayer game_player;

		game_player = this.players.get(player);
		if(game_player == null) {
			game_player = new GamePlayer(this.plugin, player);
			this.players.put(player, game_player);
		}
		return game_player;
	}

	public void removePlayer(Player player) {
		this.players.remove(player);
	}
}
