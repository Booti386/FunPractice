/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Dye;

/**
 *
 * @author guillaume
 */
public class GameInitInventory extends GameInventory {

	public GameInitInventory(FunPractice plugin, GamePlayer player) {
		super(plugin, GameInventoryType.GAME_INIT, true, player, 4 * 9);

		ItemStack[] contents = new ItemStack[4 * 9];
		ItemStack item;
		ItemMeta meta;
		Dye dye;

		/* 1st line */
		item = new ItemStack(Material.IRON_SWORD);
		meta = item.getItemMeta();
		Utils.itemMetaHideAttrsAndPotionEffects(meta);
		meta.setDisplayName("§eUnranked");
		item.setItemMeta(meta);
		contents[0] = item;

		if(plugin.getConfigManager().getRankedEnabled()) {
			item = new ItemStack(Material.DIAMOND_SWORD);
			meta = item.getItemMeta();
			Utils.itemMetaHideAttrsAndPotionEffects(meta);
			meta.setDisplayName("§cRanked");
			item.setItemMeta(meta);
		} else {
			item = new ItemStack(Material.AIR);
		}
		contents[1] = item;

		for(int i = 2; i < 6; i++)
			contents[i] = new ItemStack(Material.AIR);

		item = new ItemStack(Material.FEATHER);
		meta = item.getItemMeta();
		if (player.getFlyEnabled())
			meta.addEnchant(Enchantment.DAMAGE_ALL, 10, true);
		Utils.itemMetaHideAttrsAndPotionEffects(meta);
		meta.setDisplayName(player.getFlyEnabled()? "§aFly: On" : "§cFly: Off");
		item.setItemMeta(meta);
		contents[6] = item;

		dye = new Dye();
		dye.setColor(player.getDuelRequestDisabled() ? DyeColor.RED : DyeColor.LIME);
		item = dye.toItemStack(1);
		meta = item.getItemMeta();
		Utils.itemMetaHideAttrsAndPotionEffects(meta);
		meta.setDisplayName(player.getDuelRequestDisabled() ? "§cDuels: Off" : "§aDuels: On");
		item.setItemMeta(meta);
		contents[7] = item;

		item = new ItemStack(Material.ANVIL);
		meta = item.getItemMeta();
		Utils.itemMetaHideAttrsAndPotionEffects(meta);
		meta.setDisplayName("§aEdit kits");
		item.setItemMeta(meta);
		contents[8] = item;

		/* 2nd-4th lines */
		for(int i = 1 * 9; i < 4 * 9; i++)
			contents[i] = new ItemStack(Material.AIR);

		super.setContents(contents);
	}

}
