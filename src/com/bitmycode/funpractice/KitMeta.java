/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import java.util.Set;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

/**
 *
 * @author guillaume
 */
public class KitMeta {

	private final ItemStack helmet, chestplate, leggings, boots;
	private final ItemStack[] items;

	public KitMeta(ItemStack helmet, ItemStack chestplate, ItemStack leggings, ItemStack boots, ItemStack[] items) {
		this.helmet = helmet;
		this.chestplate = chestplate;
		this.leggings = leggings;
		this.boots = boots;
		this.items = items;
	}

	public KitMeta(PlayerInventory invt) {
		this(invt.getHelmet(), invt.getChestplate(), invt.getLeggings(), invt.getBoots(), invt.getContents());
	}

	public KitMeta() {
		this(new ItemStack(Material.AIR), new ItemStack(Material.AIR), new ItemStack(Material.AIR),  new ItemStack(Material.AIR), new ItemStack[0]);
	}

	@Override
	public String toString() {
		String str = "[ Helmet: " + this.helmet
				+ ", Chestplate: " + this.chestplate
				+ ", Leggings: " + this.leggings
				+ ", Boots: " + this.boots
				+ ", Items: [ ";

		for(int i = 0; i < this.items.length; i++)
			str += i + ": " + this.items[i] + (i < this.items.length - 1 ? ", " : " ");

		str += "]";

		return str;
	}

	public ConfigurationSection serialize() {
		ConfigurationSection section = new YamlConfiguration();
		ConfigurationSection items_section;

		section.set("Helmet", this.helmet);
		section.set("Chestplate", this.chestplate);
		section.set("Leggings", this.leggings);
		section.set("Boots", this.boots);

		items_section = section.createSection("Items");
		for(int i = 0; i < this.items.length; i++)
			items_section.set(Integer.toString(i), this.items[i]);

		return section;
	}

	public static KitMeta deserialize(ConfigurationSection section) {
		ItemStack h = new ItemStack(Material.AIR);
		ItemStack c = new ItemStack(Material.AIR);
		ItemStack l = new ItemStack(Material.AIR);
		ItemStack b = new ItemStack(Material.AIR);
		ItemStack[] itms = new ItemStack[0];

		if(section.contains("Helmet"))
			h = section.getItemStack("Helmet", h);
		if(section.contains("Chestplate"))
			c = section.getItemStack("Chestplate", c);
		if(section.contains("Leggings"))
			l = section.getItemStack("Leggings", l);
		if(section.contains("Boots"))
			b = section.getItemStack("Boots", b);

		if(section.contains("Items")) {
			ConfigurationSection items_section = section.getConfigurationSection("Items");
			Set<String> keys = items_section.getKeys(false);

			itms = new ItemStack[keys.size()];
			for(String key: keys) {
				int i;

				try {
					i = Integer.parseInt(key);
				} catch(NumberFormatException ex) {
					continue;
				}
				if(i < 0 || i >= itms.length)
					continue;
				itms[i] = items_section.getItemStack(key);
			}
		}

		return new KitMeta(h, c, l, b, itms);
	}

	public ItemStack getHelmet() {
		return this.helmet;
	}

	public ItemStack getChestplate() {
		return this.chestplate;
	}

	public ItemStack getLeggings() {
		return this.leggings;
	}

	public ItemStack getBoots() {
		return this.boots;
	}

	public ItemStack[] getItems() {
		return this.items;
	}
}
