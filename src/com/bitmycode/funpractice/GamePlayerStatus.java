/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

/**
 *
 * @author guillaume
 */
public enum GamePlayerStatus {
	IDLE,
	WAITING_GAME,
	ABOUT_TO_PLAY,
	PLAYING,
	SELECTING_DUEL,
	EDITING_KIT
}
