/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author guillaume
 */
public class GamePlayerGameMeta {

	private final GameType gameType;
	private int ELO;
	private KitMeta kit;

	public GamePlayerGameMeta(GameType game_type, int ELO, KitMeta kit_meta) {
		this.gameType = game_type;
		this.ELO = ELO;
		this.kit = kit_meta;
	}

	public GamePlayerGameMeta(GameType game_type) {
		this(game_type, 1000, new KitMeta());
	}

	@Override
	public String toString() {
		return this.gameType.name() + ": [ ELO: " + this.ELO + ", Kit: " + this.kit.toString() + " ]";
	}

	public ConfigurationSection serialize() {
		ConfigurationSection section;

		section = new YamlConfiguration().createSection(this.gameType.name());
		section.set("ELO", this.ELO);
		section.createSection("Kit", this.kit.serialize().getValues(true));

		return section;
	}

	public static GamePlayerGameMeta deserialize(ConfigurationSection section) {
		GameType game_type;
		int ELO = 1000;
		KitMeta kit = new KitMeta();

		game_type = GameType.valueOfNoExcept(section.getName());
		if(game_type == null)
			return null;

		if(section.contains("ELO"))
			ELO = section.getInt("ELO");

		if(section.contains("Kit"))
			kit = KitMeta.deserialize(section.getConfigurationSection("Kit"));

		return new GamePlayerGameMeta(game_type, ELO, kit);
	}

	public GameType getGameType() {
		return this.gameType;
	}

	public int getELO() {
		return this.ELO;
	}

	public void setELO(int ELO) {
		this.ELO = ELO;
	}

	public KitMeta getKit() {
		return this.kit;
	}

	public void setKit(KitMeta kit) {
		this.kit = kit;
	}

}
