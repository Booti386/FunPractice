/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

/**
 *
 * @author guillaume
 */
public enum GameInventoryType {
	GAME_INIT,
	GAME_SELECT,
	GAME_LEAVE,
	GAME_END,
	GAME_KIT_SELECT
}
