/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import java.util.Arrays;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author guillaume
 */
public class FunPractice extends JavaPlugin {

	private Server server;
	private Logger logger;
	private World templateWorld;
	private World world;
	private ConfigManager configMgr;
	private DuelManager duelMgr;
	private GameManager gameMgr;
	private GamePlayerManager gamePlayerMgr;
	private EventListener evtListener;

	@Override
	public void onLoad() {
		this.server = this.getServer();
		this.logger = this.getLogger();
		this.getDataFolder().mkdirs();
		this.configMgr = new ConfigManager(this);
		this.duelMgr = new DuelManager(this);
		this.gameMgr = new GameManager(this);
		this.gamePlayerMgr = new GamePlayerManager(this);
	}

	private World createEmptyWorld(String name) {
		World w;

		w = Utils.setGeneratorSettings(new WorldCreator(name), "1;0")
				.environment(Environment.NORMAL)
				.generateStructures(false)
				.type(WorldType.FLAT)
				.createWorld();
		w.setSpawnFlags(false, false);
		w.setAmbientSpawnLimit(0);
		w.setAnimalSpawnLimit(0);
		w.setMonsterSpawnLimit(0);
		w.setAutoSave(true);
		w.getBlockAt(0, 0, 0).setType(Material.STONE);
		return w;
	}

	@Override
	public void onEnable() {
		this.templateWorld = this.server.getWorld("FunPracticeTemplateWorld");
		if(this.templateWorld == null)
			this.templateWorld = createEmptyWorld("FunPracticeTemplateWorld");

		this.templateWorld.getLivingEntities().forEach((entity) -> {
			if(entity.getType() != EntityType.PLAYER)
				entity.setHealth(0);
		});

		this.world = this.server.getWorld("FunPracticeWorld");
		if(this.world == null)
			this.world = createEmptyWorld("FunPracticeWorld");

		Location loc = this.configMgr.getMapSpawn().toLocation(this.world);
		this.world.setSpawnLocation(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
		this.world.setKeepSpawnInMemory(true);

		this.world.getLivingEntities().forEach((entity) -> {
			if(entity.getType() != EntityType.PLAYER)
				entity.setHealth(0);
		});

		this.evtListener = new EventListener(this);
		this.server.getPluginManager().registerEvents(this.evtListener, this);
		this.server.getOnlinePlayers().forEach((player) -> {
			this.evtListener.onPlayerJoin(new PlayerJoinEvent(player, ""));
		});
	}

	@Override
	public void onDisable() {
		this.server.getOnlinePlayers().forEach((player) -> {
			this.evtListener.onPlayerQuit(new PlayerQuitEvent(player, ""));
		});
		HandlerList.unregisterAll(this);
		this.configMgr.saveConfig();
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		final String command_name = command.getName().toLowerCase();
		Player p = null, p1;
		GamePlayer player = null, player1;
		GameType game_type;
		Game game;
		Duel duel;
		ItemStack item;
		ItemMeta meta;
		UUID uuid;
		int elo;

		if(sender instanceof Player)
			p = (Player)sender;
		if(p != null)
			player = this.gamePlayerMgr.getPlayer(p);

		switch(command_name) {
			case "control-toggle":
				if(player != null && p != null) {
					this.evtListener.onPlayerQuit(new PlayerQuitEvent(p, ""));
					p.sendMessage("§aVous vous êtes émancipé du serveur !");
				} else if(p != null) {
					this.evtListener.onPlayerJoin(new PlayerJoinEvent(p, ""));
					p.sendMessage("§aVous avez été rattaché au serveur !");
				} else
					return false;
				return true;

			case "duel":
				if(player == null)
					return false;

				if(args.length != 1)
					return false;

				p1 = this.server.getPlayer(args[0]);
				if(p1 == null)
					return false;

				player1 = this.gamePlayerMgr.getPlayer(p1);
				if(player1 == null)
					return false;

				if(player == player1) {
					player.sendMessage("§cVous ne pouvez pas vous combattre vous-même");
					return true;
				}

				if(player1.getDuelRequestDisabled()) {
					player.sendMessage("§cCe joueur a désactivé ses demandes de duels");
					return true;
				}

				if(player.getStatus() != GamePlayerStatus.IDLE || player1.getStatus() != GamePlayerStatus.IDLE) {
					player.sendMessage("§cVous ne pouvez pas combattre ce joueur pour le moment");
					return true;
				}

				player.setStatus(GamePlayerStatus.SELECTING_DUEL);
				this.duelMgr.addCachedDuel(player, new Duel(null, player, player1));
				player.openInventory(new GameSelectInventory(this, player, player1, false));
				return true;

			case "duel-accept":
				int duel_id = -1;

				if(player == null)
					return false;

				if(args.length != 1)
					return false;

				try {
					duel_id = Integer.parseInt(args[0]);
					duel = this.duelMgr.getDuel(duel_id);
				} catch(NumberFormatException ex) {
					duel = null;
				}
				if(duel == null)
					return false;

				if(player != duel.getTarget())
					return false;

				if(player.getStatus() != GamePlayerStatus.IDLE || duel.getAsker().getStatus() != GamePlayerStatus.IDLE) {
					player.sendMessage("§cVous ne pouvez pas combattre ce joueur pour le moment");
					return true;
				}

				this.duelMgr.removeDuel(duel_id);
				this.gameMgr.registerPlayersToNewGame(duel.getType(), duel.getAsker(), duel.getTarget());
				return true;

			case "duel-toggle":
				boolean duel_request_disabled;

				if(player == null)
					return false;

				duel_request_disabled = !player.getDuelRequestDisabled();
				player.setDuelRequestDisabled(duel_request_disabled);
				if(duel_request_disabled)
					player.sendMessage("§cRéception de requêtes de duel désactivée.");
				else
					player.sendMessage("§aRéception de requêtes de duel activée.");
				return true;

			case "elo":
				if(player == null)
					return false;

				if(!this.configMgr.getRankedEnabled()) {
					player.sendMessage("§cRanked désactivé sur ce serveur");
					return true;
				}

				switch (args.length) {
					case 0:
						player1 = player;
						break;

					case 1:
						p1 = this.server.getPlayer(args[0]);
						if(p1 == null)
							return false;

						player1 = this.gamePlayerMgr.getPlayer(p1);
						if(player1 == null)
							return false;
						break;

					default:
						return false;
				}

				final GamePlayer player_ = player;
				final GamePlayer player1_ = player1;
				player.sendMessage("Elos de " + player1.getName() + " :");
				Arrays.stream(GameType.values()).forEach((game_type_) -> {
					if(game_type_.getRanked())
						player_.sendMessage(game_type_.toString() + " : " + player1_.getELO(game_type_) + " " + player1_.getRank(game_type_));
				});
				return true;

			case "fly-toggle":
				boolean fly_enabled;

				if(player == null)
					return false;

				if(player.getStatus() != GamePlayerStatus.IDLE) {
					player.sendMessage("§cFly non disponible");
					return true;
				}

				fly_enabled = !player.getFlyEnabled();
				player.setFlyEnabled(fly_enabled);
				if(fly_enabled)
					player.sendMessage("§aFly activé.");
				else
					player.sendMessage("§cFly désactivé.");
				return true;

			case "practice-infos":
				if(p == null)
					return false;

				p.sendMessage("Arènes libres : " + this.configMgr.getFreeArenaCount() + "/" + this.configMgr.getArenaCount());
				p.sendMessage("Jeux en attente : " + this.gameMgr.getWaitingGames().length);
				p.sendMessage("Jeux en lancement : " + this.gameMgr.getLaunchingGames().length);
				p.sendMessage("Jeux en cours : " + this.gameMgr.getLaunchedGames().length);
				p.sendMessage("Jeux terminés : " + this.gameMgr.getCompletedGames().length);
				p.sendMessage("Jeux avortés : " + this.gameMgr.getAbortedGames().length);
				p.sendMessage("Joueurs (non-émancipés) : " + this.gamePlayerMgr.getPlayerCount());
				p.sendMessage("Demandes de duel en attente : " + this.duelMgr.getDuelCount());
				return true;

			case "reset":
				if (args.length < 1)
					return false;

				switch (args[0])
				{
					case "arenas":
						this.configMgr.getArenas().forEach((arena) -> {
							arena.setNeedsFullCleanup(true);
						});
						break;

					case "arena":
						if (args.length < 2)
							return false;

						this.configMgr.getArenas().stream()
								.filter((arena) -> arena.getName().equals(args[1]))
								.forEach((arena) -> {
									arena.setNeedsFullCleanup(true);
								});
						break;
				}
				return true;

			case "setelo":
				if(args.length != 3)
					return false;

				p1 = this.server.getPlayer(args[0]);
				if(p1 == null)
					return false;

				player1 = this.gamePlayerMgr.getPlayer(p1);
				if(player1 == null)
					return false;

				game_type = GameType.valueOfNoExcept(args[1]);
				if(game_type == null)
					return false;

				try {
					elo = Integer.parseInt(args[2]);
				} catch(NumberFormatException ex) {
					return false;
				}

				player1.setELO(game_type, elo);
				if(player != null)
					player.sendMessage("§aELO modifié avec succès à la valeur " + elo + " pour le joueur " + player1.getName() + " dans le mode de jeu " + game_type.toString() + ".");
				return true;

			case "setkit":
				if(p == null)
					return false;

				if(args.length != 1)
					return false;

				game_type = GameType.valueOfNoExcept(args[0]);
				if(game_type == null)
					return false;

				this.configMgr.replaceKit(game_type, new KitMeta(p.getInventory()));
				this.configMgr.saveConfig(false, false, true, false);
				return true;

			case "showblockscoords":
				if(p == null)
					return false;

				/* The player must be detached from the server. */
				if(player != null) {
					p.sendMessage("§cYou don't have the rights to execute this command (you must be emancipated).");
					return true;
				}

				item = new ItemStack(Material.STICK);
				meta = item.getItemMeta();
				meta.setDisplayName("§aCoordonnées du bloc");
				item.setItemMeta(meta);
				p.getInventory().addItem(item);
				p.updateInventory();;
				return true;

			case "showgameinventory":
				if(player == null)
					return false;

				if(args.length != 2)
					return false;

				game = player.getGame();
				if(game != null && game.getStatus() != GameStatus.COMPLETED)
					return true; /* Just don't do anything */

				game = this.gameMgr.getGame(Integer.parseInt(args[0]));
				if(game == null)
					return false;

				try {
					uuid = UUID.fromString(args[1]);
				} catch(IllegalArgumentException ex) {
					return false;
				}

				game.showSavedInventory(player, uuid);
				return true;

			case "tptodefault":
				if(args.length != 0)
					return false;

				if(player != null && p != null) {
					game = player.getGame();
					if(game != null && game.getStatus() != GameStatus.COMPLETED)
						return false;
				} else if(p == null)
					return false;

				p.teleport(this.server.getWorlds().get(0).getSpawnLocation());
				return true;

			case "tptonormal":
				if(args.length != 0)
					return false;

				if(player != null && p != null) {
					game = player.getGame();
					if(game != null && game.getStatus() != GameStatus.COMPLETED)
						return false;
				} else if(p == null)
					return false;

				p.teleport(this.configMgr.getMapSpawn().toLocation(this.world));
				return true;

			case "tptotemplate":
				if(args.length != 0)
					return false;

				if(player != null && p != null) {
					game = player.getGame();
					if(game != null && game.getStatus() != GameStatus.COMPLETED)
						return false;
				} else if(p == null)
					return false;

				p.teleport(this.configMgr.getMapSpawn().toLocation(this.templateWorld));
				return true;
		}
		return false;
	}

	public void log(String str) {
		this.logger.log(Level.INFO, str);
	}

	public void log(String str, Throwable thr) {
		this.logger.log(Level.INFO, str, thr);
	}

	public void warn(String str) {
		this.logger.log(Level.WARNING, str);
	}

	public void warn(String str, Throwable thr) {
		this.logger.log(Level.WARNING, str, thr);
	}

	public void error(String str) {
		this.logger.log(Level.SEVERE, str);
	}

	public void error(String str, Throwable thr) {
		this.logger.log(Level.SEVERE, str, thr);
	}

	public World getTemplateWorld() {
		return this.templateWorld;
	}

	public World getWorld() {
		return this.world;
	}

	public ConfigManager getConfigManager() {
		return this.configMgr;
	}

	public DuelManager getDuelManager() {
		return this.duelMgr;
	}

	public GameManager getGameManager() {
		return this.gameMgr;
	}

	public GamePlayerManager getPlayerManager() {
		return this.gamePlayerMgr;
	}

}
