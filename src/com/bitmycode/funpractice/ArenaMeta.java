/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author guillaume
 */
public class ArenaMeta {

	private final String name;
	private final SpawnMeta spawn1, spawn2;
	private final RectMeta coords;
	private boolean needsFullCleanup;
	private boolean needsCleanup;

	public ArenaMeta(String name, SpawnMeta spawn1, SpawnMeta spawn2, RectMeta coords) {
		this.name = name;
		this.spawn1 = spawn1;
		this.spawn2 = spawn2;
		this.coords = coords;
		this.needsFullCleanup = true;
		this.needsCleanup = false;
	}

	public ArenaMeta() {
		this("", new SpawnMeta(), new SpawnMeta(), new RectMeta());
	}

	@Override
	public String toString() {
		return this.name + ": [ Spawns: [ 1: " + this.spawn1 + ", 2: " + this.spawn2 + " ], Coords: " + this.coords + " ]";
	}

	public ConfigurationSection serialize() {
		ConfigurationSection section;
		ConfigurationSection spawns_section;

		section = new YamlConfiguration().createSection(this.name);
		spawns_section = section.createSection("Spawns");
		spawns_section.createSection("1", this.spawn1.serialize().getValues(true));
		spawns_section.createSection("2", this.spawn2.serialize().getValues(true));
		section.createSection("Coords", this.coords.serialize().getValues(true));

		return section;
	}

	public static ArenaMeta deserialize(ConfigurationSection section) {
		String name = section.getName();
		SpawnMeta s1 = new SpawnMeta(), s2 = new SpawnMeta();
		RectMeta c1 = new RectMeta();

		if(section.contains("Spawns")) {
			ConfigurationSection spawns_section = section.getConfigurationSection("Spawns");

			if(spawns_section.contains("1"))
				s1 = SpawnMeta.deserialize(spawns_section.getConfigurationSection("1"));

			if(spawns_section.contains("2"))
				s2 = SpawnMeta.deserialize(spawns_section.getConfigurationSection("2"));
		}

		if(section.contains("Coords"))
			c1 = RectMeta.deserialize(section.getConfigurationSection("Coords"));

		return new ArenaMeta(name, s1, s2, c1);
	}

	public String getName() {
		return this.name;
	}

	public SpawnMeta[] getSpawns() {
		return new SpawnMeta[]{this.spawn1, this.spawn2};
	}

	public RectMeta getCoords() {
		return this.coords;
	}

	public boolean getNeedsFullCleanup() {
		return this.needsFullCleanup;
	}

	public void setNeedsFullCleanup(boolean needs_full_cleanup) {
		this.needsFullCleanup = needs_full_cleanup;
	}

	public boolean getNeedsCleanup() {
		return this.needsCleanup;
	}

	public void setNeedsCleanup(boolean needs_cleanup) {
		this.needsCleanup = needs_cleanup;
	}

}
