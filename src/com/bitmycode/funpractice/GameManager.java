/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import java.util.AbstractMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 *
 * @author guillaume
 */
public class GameManager {

	private final FunPractice plugin;
	private int id;
	private final ConcurrentMap<Integer, Game> games;

	public GameManager(FunPractice plugin) {
		this.plugin = plugin;
		this.id = 0;
		this.games = new ConcurrentHashMap<>();
	}

	private void registerGame(Game game) {
		int game_id = this.id++;

		this.games.put(game_id, game);

		removeCompletedGames(getCompletedGamesCount() - this.plugin.getConfigManager().getMaxCompletedGames());
	}

	public void registerPlayer(GameType type, GamePlayer player) {
		Game game;

		game = this.games.values().stream()
				.filter((entry) -> entry.getStatus() == GameStatus.WAITING && entry.getType() == type)
				.findAny()
				.orElse(null);
		if(game == null)
			this.registerGame(game = new Game(this.plugin, type));

		if(game.isFull())
			return;

		game.addPlayer(player);
	}

	public void registerPlayersToNewGame(GameType type, GamePlayer player, GamePlayer player1) {
		Game game;

		this.registerGame(game = new Game(this.plugin, type));
		game.addPlayer(player);
		game.addPlayer(player1);
	}

	public void removePlayer(GamePlayer player) {
		Game game = player.getGame();

		if(game == null)
			return;
		game.removePlayer(player);
	}

	public void killPlayer(GamePlayer player) {
		removePlayer(player);
	}

	public Game getGame(int id) {
		return this.games.getOrDefault(id, null);
	}

	private Stream<Map.Entry<Integer, Game>> gamesFilterEntries(Predicate<Map.Entry<Integer, Game>> predicate) {
		return this.games.entrySet().stream().filter(predicate);
	}

	private Stream<Game> gamesFilter(Predicate<Game> predicate) {
		return this.games.values().stream().filter(predicate);
	}

	private Game[] gamesFilterToArray(Predicate<Game> predicate) {
		return gamesFilter(predicate).toArray(Game[]::new);
	}

	public int getGameID(Game game) {
		return gamesFilterEntries((entry) -> entry.getValue() == game)
				.findAny()
				.orElse(new AbstractMap.SimpleImmutableEntry<>(null, null))
				.getKey();
	}

	public Game[] getWaitingGames() {
		return gamesFilterToArray((game) -> game.getStatus() == GameStatus.WAITING);
	}

	public Game[] getWaitingGames(GameType type) {
		return gamesFilterToArray((game) -> game.getStatus() == GameStatus.WAITING
				&& game.getType() == type);
	}

	public Game[] getLaunchingGames() {
		return gamesFilterToArray((game) -> game.getStatus() == GameStatus.LAUNCHING
				|| game.getStatus() == GameStatus.PRELAUNCHED);
	}

	public Game[] getWaitingOrLaunchingGames(GameType type) {
		return gamesFilterToArray((game) -> (game.getStatus() == GameStatus.WAITING
					|| game.getStatus() == GameStatus.LAUNCHING
					|| game.getStatus() == GameStatus.PRELAUNCHED)
				&& game.getType() == type);
	}

	public Game[] getLaunchedGames() {
		return gamesFilterToArray((game) -> game.getStatus() == GameStatus.LAUNCHED);
	}

	public Game[] getLaunchedGames(GameType type) {
		return gamesFilterToArray((game) -> game.getStatus() == GameStatus.LAUNCHED
				&& game.getType() == type);
	}

	public Game[] getCompletedGames() {
		return gamesFilterToArray((game) -> game.getStatus() == GameStatus.COMPLETED);
	}

	public int getCompletedGamesCount() {
		return (int)gamesFilter((game) -> game.getStatus() == GameStatus.COMPLETED)
				.count();
	}

	public Game getFirstCompletedGame() {
		return gamesFilter((game) -> game.getStatus() == GameStatus.COMPLETED)
				.findFirst()
				.orElse(null);
	}

	public Game[] getAbortedGames() {
		return gamesFilterToArray((game) -> (game.getStatus() == GameStatus.ABORTED_0
				|| game.getStatus() == GameStatus.ABORTED_1));
	}

	public void removeCompletedGames(int count) {
		count = Math.max(count, 0);

		gamesFilterEntries((entry) -> entry.getValue().getStatus() == GameStatus.COMPLETED)
				.sorted((o1, o2) -> {
					return o1.getKey() - o2.getKey();
				})
				.limit(count)
				.forEach((entry) -> {
					this.games.remove(entry.getKey());
				});
	}

}
