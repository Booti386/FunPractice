/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author guillaume
 */
public class GamePlayerMeta
{
	private final UUID uuid;
	private final Map<GameType, GamePlayerGameMeta> games;
	private boolean duelRequestDisabled;
	private int currentRankedCount;
	private long lastRankedTS;

	public GamePlayerMeta(UUID uuid, Map<GameType, GamePlayerGameMeta> games, boolean duel_request_disabled, int current_ranked_count, long last_ranked_ts)
	{
		this.uuid = uuid;
		this.games = games;
		this.duelRequestDisabled = duel_request_disabled;
		this.currentRankedCount = current_ranked_count;
		this.lastRankedTS = last_ranked_ts;
	}

	public GamePlayerMeta(UUID uuid)
	{
		this(uuid, new HashMap<>(), false, 0, 0);

		Arrays.stream(GameType.values()).forEach((game_type) -> {
			this.games.put(game_type, new GamePlayerGameMeta(game_type));
		});
	}

	@Override
	public String toString()
	{
		String str = this.uuid.toString() + ": [ Games: [ ";

		str += this.games.entrySet().stream()
				.map((s) -> {
					GamePlayerGameMeta game = s.getValue();

					return game + ", ";
				})
				.collect(Collectors.joining());

		str += "], DuelRequestDisabled: " + this.duelRequestDisabled
				+ ", CurrentRankedCount: " + this.currentRankedCount
				+ ", LastRankedTS: " + this.lastRankedTS
				+ " ]";

		return str;
	}

	public ConfigurationSection serialize()
	{
		ConfigurationSection section = new YamlConfiguration().createSection(this.uuid.toString());
		ConfigurationSection kits_section;

		kits_section = section.createSection("Games");
		this.games.entrySet().stream().forEach((s) -> {
			GameType game_type = s.getKey();
			GamePlayerGameMeta game = s.getValue();
			ConfigurationSection game_section = game.serialize();

			kits_section.set(game_type.name(), game_section.getValues(false));
		});

		section.set("DuelRequestDisabled", this.duelRequestDisabled);
		section.set("CurrentRankedCount", this.currentRankedCount);
		section.set("LastRankedTS", this.lastRankedTS);

		return section;
	}

	public static GamePlayerMeta deserialize(ConfigurationSection section)
	{
		GamePlayerMeta meta;
		UUID uuid;
		ConfigurationSection games_section = null;

		try {
			uuid = UUID.fromString(section.getName());
		} catch (IllegalArgumentException ex) {
			return null;
		}

		meta = new GamePlayerMeta(uuid);

		if (section.contains("Games"))
			games_section = section.getConfigurationSection("Games");

		if (games_section == null)
			games_section = new YamlConfiguration();

		final ConfigurationSection gs = games_section;
		Arrays.stream(GameType.values()).forEach((game_type) -> {
			ConfigurationSection game_section = gs.getConfigurationSection(game_type.name());
			GamePlayerGameMeta game_meta = null;

			if (game_section != null)
			{
				game_meta = GamePlayerGameMeta.deserialize(game_section);
				if (game_meta != null && game_meta.getGameType() != game_type)
					game_meta = null;
			}

			if (game_meta == null)
				game_meta = new GamePlayerGameMeta(game_type);

			meta.setPlayerGameMeta(game_type, game_meta);
		});

		if (section.isBoolean("DuelRequestDisabled"))
			meta.setDuelRequestDisabled(section.getBoolean("DuelRequestDisabled"));

		if (section.isInt("CurrentRankedCount"))
			meta.setCurrentRankedCount(section.getInt("CurrentRankedCount"));

		if (section.isLong("LastRankedTS"))
			meta.setLastRankedTS(section.getLong("LastRankedTS"));

		return meta;
	}

	public UUID getUUID()
	{
		return this.uuid;
	}

	public GamePlayerGameMeta getPlayerGameMeta(GameType game_type)
	{
		return this.games.get(game_type);
	}

	public void setPlayerGameMeta(GameType game_type, GamePlayerGameMeta meta)
	{
		this.games.put(game_type, meta);
	}

	public int getELO(GameType game_type)
	{
		return getPlayerGameMeta(game_type).getELO();
	}

	public void setELO(GameType game_type, int ELO)
	{
		getPlayerGameMeta(game_type).setELO(ELO);
	}

	public KitMeta getKit(GameType game_type)
	{
		return getPlayerGameMeta(game_type).getKit();
	}

	public void setKit(GameType game_type, KitMeta kit)
	{
		getPlayerGameMeta(game_type).setKit(kit);
	}

	public boolean getDuelRequestDisabled()
	{
		return this.duelRequestDisabled;
	}

	public void setDuelRequestDisabled(boolean duel_request_disabled)
	{
		this.duelRequestDisabled = duel_request_disabled;
	}

	public int getCurrentRankedCount()
	{
		return this.currentRankedCount;
	}

	public void setCurrentRankedCount(int current_ranked_count)
	{
		this.currentRankedCount = current_ranked_count;
	}

	public long getLastRankedTS()
	{
		return this.lastRankedTS;
	}

	public void setLastRankedTS(long last_ranked_ts)
	{
		this.lastRankedTS = last_ranked_ts;
	}

}
