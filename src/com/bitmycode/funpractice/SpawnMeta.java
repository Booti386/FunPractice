/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author guillaume
 */
public class SpawnMeta {

	private double x, y, z;
	private float yaw, pitch;

	public SpawnMeta(double x, double y, double z, float yaw, float pitch) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.yaw = yaw;
		this.pitch = pitch;
	}

	public SpawnMeta() {
		this(0., 0., 0., 0.f, 0.f);
	}

	@Override
	public String toString() {
		return "[ X=" + this.x + ", Y=" + this.y + ", Z=" + this.z + ", YAW=" + this.yaw + ", Pitch=" + this.pitch + " ]";
	}

	public ConfigurationSection serialize() {
		ConfigurationSection section = new YamlConfiguration();

		section.set("X", this.x);
		section.set("Y", this.y);
		section.set("Z", this.z);
		section.set("YAW", this.yaw);
		section.set("Pitch", this.pitch);
		return section;
	}

	public static SpawnMeta deserialize(ConfigurationSection section) {
		return new SpawnMeta(
			section.getDouble("X", 0),
			section.getDouble("Y", 0),
			section.getDouble("Z", 0),
			(float)section.getDouble("YAW", 0),
			(float)section.getDouble("Pitch", 0));
	}

	public Location toLocation(World world) {
		return new Location(world, this.x, this.y, this.z, this.yaw, this.pitch);
	}

}
