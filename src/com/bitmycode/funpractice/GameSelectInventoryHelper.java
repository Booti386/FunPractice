/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import java.util.Arrays;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author guillaume
 */
public class GameSelectInventoryHelper extends GameInventory {

	/* Export the original constructor */
	protected GameSelectInventoryHelper(FunPractice plugin, GameInventoryType type, boolean replace_player_inventory, GamePlayer player, int nb_items, String title) {
		super(plugin, type, replace_player_inventory, player, nb_items, title);
	}

	protected final void itemStackSetName(ItemStack item, String name) {
		ItemMeta meta = item.getItemMeta();
		Utils.itemMetaHideAttrsAndPotionEffects(meta);
		meta.setDisplayName("§6" + name);
		item.setItemMeta(meta);
	}

	protected final void itemStackSetLore(ItemStack item, String[] lore) {
		ItemMeta meta = item.getItemMeta();
		meta.setLore(Arrays.asList(lore));
		item.setItemMeta(meta);
	}

	public GameType getRankedGameType(int slot) {
		switch(slot) {
			case 0:
				return GameType.RANKED_NO_DEBUFF;

			case 1:
				return GameType.RANKED_BUILD_UHC;

			case 2:
				return GameType.RANKED_BUILD_POT;

			case 3:
				return GameType.RANKED_G_APPLE;

			case 4:
				return GameType.RANKED_ARCHER;

			case 5:
				return GameType.RANKED_IRON;
		}
		return null;
	}

	public GameType getUnrankedGameType(int slot) {
		switch(slot) {
			case 0:
				return GameType.NO_DEBUFF;

			case 1:
				return GameType.BUILD_UHC;

			case 2:
				return GameType.BUILD_POT;

			case 3:
				return GameType.G_APPLE;

			case 4:
				return GameType.ARCHER;

			case 5:
				return GameType.IRON;

			case 6:
				return GameType.HC_NO_ENCHANTS;

			case 7:
				return GameType.SG;
		}
		return null;
	}

}
