/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import java.util.Date;
import java.util.UUID;
import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

/**
 *
 * @author guillaume
 */
public class GamePlayer {
	private final FunPractice plugin;
	private final Player player;
	private GamePlayerStatus status;
	private GameType editKitType;
	private Game game;
	private SpawnMeta spawn;
	private GameInventory openInventory;
	private final GamePlayerMeta meta;
	private boolean fly_enabled;

	public GamePlayer(FunPractice plugin, Player player) {
		this.plugin = plugin;
		this.player = player;
		this.status = GamePlayerStatus.IDLE;
		this.editKitType = GameType.NO_DEBUFF;
		this.game = null;
		this.spawn = null;
		this.openInventory = null;
		this.meta = this.plugin.getConfigManager().createPlayerIfNotExists(player.getUniqueId());
		this.fly_enabled = false;
	}

	public Player getPlayer() {
		return this.player;
	}

	public GamePlayerStatus getStatus() {
		return this.status;
	}

	public GamePlayer setStatus(GamePlayerStatus status) {
		this.status = status;
		return this;
	}

	public GameType getEditKitType() {
		return this.editKitType;
	}

	public GamePlayer setEditKitType(GameType edit_kit_type) {
		this.editKitType = edit_kit_type;
		return this;
	}

	public Game getGame() {
		return this.game;
	}

	public GamePlayer setGame(Game game) {
		this.game = game;
		return this;
	}

	public SpawnMeta getSpawn() {
		return this.spawn;
	}

	public GamePlayer setSpawn(SpawnMeta spawn) {
		this.spawn = spawn;
		return this;
	}

	public GameInventory getOpenInventory() {
		return this.openInventory;
	}

	public int getELO(GameType game_type) {
		return this.meta.getELO(game_type);
	}

	public GamePlayer setELO(GameType game_type, int ELO) {
		this.meta.setELO(game_type, ELO);
		this.plugin.getConfigManager().saveConfig(this.meta);
		return this;
	}

	public boolean getDuelRequestDisabled() {
		return this.meta.getDuelRequestDisabled();
	}

	public GamePlayer setDuelRequestDisabled(boolean elo_request_disabled) {
		this.meta.setDuelRequestDisabled(elo_request_disabled);
		this.plugin.getConfigManager().saveConfig(this.meta);
		return this;
	}

	public boolean getFlyEnabled() {
		return this.fly_enabled;
	}

	public GamePlayer setFlyEnabled(boolean fly_enabled) {
		this.player.setAllowFlight(fly_enabled);
		this.fly_enabled = fly_enabled;
		return this;
	}

	public KitMeta getKit(GameType game_type) {
		return this.meta.getKit(game_type);
	}

	public GamePlayer setKit(GameType game_type, KitMeta kit) {
		this.meta.setKit(game_type, kit);
		return this;
	}

	public String getName() {
		String name = this.player.getName();

		if(name == null)
			return "<" + this.player.getUniqueId().toString() + ">";
		return name;
	}

	public String getRank(GameType game_type) {
		return Utils.ELOToRank(this.getELO(game_type));
	}

	public String getRankAndName(GameType game_type) {
		return getRank(game_type) + " " + getName();
	}

	public UUID getUniqueId() {
		return this.player.getUniqueId();
	}

	public void resetState() {
		PlayerInventory inventory = this.player.getInventory();

		this.setStatus(GamePlayerStatus.IDLE);
		this.player.setHealth(this.player.getMaxHealth());
		this.player.setFoodLevel(20);
		this.player.setFireTicks(0);
		removeAllPotionEffects();

		inventory.clear();
		inventory.setArmorContents(null);
		openInventory(new GameInitInventory(this.plugin, this));

		this.player.updateInventory();
	}

	public GamePlayer teleportToSpawn() {
		this.teleport(this.plugin.getConfigManager().getMapSpawn());
		return this;
	}

	public GamePlayer teleport(SpawnMeta spawn) {
		this.player.teleport(spawn.toLocation(this.plugin.getWorld()));
		return this;
	}

	public GamePlayer sendMessage(String message) {
		this.player.sendMessage(message);
		return this;
	}

	public GamePlayer sendMessage(BaseComponent message) {
		this.player.spigot().sendMessage(message);
		return this;
	}

	public GamePlayer openInventory(GameInventory inventory) {
		closeInventory(() -> {
			this.openInventory = inventory;
			this.openInventory.open();
		});
		return this;
	}

	public GamePlayer closeInventory(Runnable task) {
		if(this.openInventory != null) {
			this.openInventory.setOnClose(task);
			if(this.openInventory.anticipatedClose())
				doCloseInventory();
		} else {
			if(task != null)
				task.run();
		}
		return this;
	}

	public GamePlayer closeInventory() {
		closeInventory(null);
		return this;
	}

	public GamePlayer doCloseInventory() {
		if(this.openInventory != null) {
			GameInventory open_invt = this.openInventory;

			this.openInventory = null;
			open_invt.close();
		}
		return this;
	}

	public GamePlayer clearInventory() {
		this.player.getInventory().clear();
		this.player.updateInventory();
		return this;
	}

	public PlayerInventory getInventory() {
		return this.player.getInventory();
	}

	public GamePlayer setInventory(KitMeta kit) {
		PlayerInventory invt = this.player.getInventory();

		invt.setContents(kit.getItems());
		invt.setHelmet(kit.getHelmet());
		invt.setChestplate(kit.getChestplate());
		invt.setLeggings(kit.getLeggings());
		invt.setBoots(kit.getBoots());
		this.player.updateInventory();
		return this;
	}

	public GamePlayer removeAllPotionEffects() {
		this.player.getActivePotionEffects().stream().forEach((effect) -> {
			this.player.removePotionEffect(effect.getType());
		});
		return this;
	}

	private static double pD(int D) {
		return 1.0 / (1.0 + 10.0 * Math.exp(-D / 100.0));
	}

	public static int updateELO(GamePlayer winner, GamePlayer looser) {
		GameType game_type = winner.getGame().getType();
		int winner_ELO = winner.getELO(game_type);
		int looser_ELO = looser.getELO(game_type);
		final double K = 26.0;
		final double winner_W = 1.0;
		final double winner_pD = pD(winner_ELO - looser_ELO);
		final int ELO_delta = Utils.clamp(K * (winner_W - winner_pD), 1.0, 25.0).intValue();

		winner_ELO += ELO_delta;
		looser_ELO = Math.max(looser_ELO - ELO_delta, 0);
		winner.setELO(game_type, winner_ELO);
		looser.setELO(game_type, looser_ELO);
		return ELO_delta;
	}

	private void checkResetRankedCount()
	{
		if (Utils.truncateDateToDay(new Date()).compareTo(Utils.truncateDateToDay(new Date(this.meta.getLastRankedTS()))) != 0)
		{
			this.meta.setCurrentRankedCount(0);
			this.plugin.getConfigManager().saveConfig(this.meta);
		}
	}

	public boolean isRankedAllowed()
	{
		int max_ranked = 0;

		checkResetRankedCount();

		for (RankedMatchesLimitType lvl : RankedMatchesLimitType.values())
		{
			String lvl_name = lvl.name();
			String perm = "FunPractice.RankedMatchesLimit." + lvl_name;

			if (this.getPlayer().hasPermission(perm))
			{
				int max = this.plugin.getConfigManager().getMaxRankedMatches(lvl);

				if (max > max_ranked)
					max_ranked = max;
			}
		}

		return this.meta.getCurrentRankedCount() < max_ranked;
	}

	public int incrementRankedCount()
	{
		checkResetRankedCount();

		this.meta.setCurrentRankedCount(this.meta.getCurrentRankedCount() + 1);
		this.meta.setLastRankedTS(new Date().getTime());
		this.plugin.getConfigManager().saveConfig(this.meta);

		return this.meta.getCurrentRankedCount();
	}

}
