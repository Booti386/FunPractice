/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author guillaume
 */
public class RectMeta {

	private int minX, minZ, maxX, maxZ;

	public RectMeta(int min_x, int min_z, int max_x, int max_z) {
		this.minX = min_x;
		this.minZ = min_z;
		this.maxX = max_x;
		this.maxZ = max_z;
	}

	public RectMeta() {
		this(0, 0, 0, 0);
	}

	@Override
	public String toString() {
		return "[ MinX=" + this.minX + ", MinZ=" + this.minZ + ", MaxX=" + this.maxX + ", MaxZ=" + this.maxZ + " ]";
	}

	public ConfigurationSection serialize() {
		ConfigurationSection section = new YamlConfiguration();

		section.set("MinX", this.minX);
		section.set("MinZ", this.minZ);
		section.set("MaxX", this.maxX);
		section.set("MaxZ", this.maxZ);
		return section;
	}

	public static RectMeta deserialize(ConfigurationSection section) {
		return new RectMeta(
			section.getInt("MinX", 0),
			section.getInt("MinZ", 0),
			section.getInt("MaxX", 0),
			section.getInt("MaxZ", 0));
	}

	public int getMinX() {
		return this.minX;
	}

	public int getMinZ() {
		return this.minZ;
	}

	public int getMaxX() {
		return this.maxX;
	}

	public int getMaxZ() {
		return this.maxZ;
	}

}
