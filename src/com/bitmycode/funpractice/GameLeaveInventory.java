/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bitmycode.funpractice;

import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Dye;

/**
 *
 * @author guillaume
 */
public class GameLeaveInventory extends GameInventory {

	public GameLeaveInventory(FunPractice plugin, GamePlayer player) {
		super(plugin, GameInventoryType.GAME_LEAVE, true, player, 4 * 9);

		ItemStack[] contents = new ItemStack[4 * 9];
		int i = 0;

		/* 1st line */
		Dye dye = new Dye();
		dye.setColor(DyeColor.ORANGE);
		ItemStack item = dye.toItemStack(1);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName("§cQuitter la file d'attente");
		item.setItemMeta(meta);
		contents[i++] = item;

		/* 2nd-4th lines */
		while(i < 4 * 9)
			contents[i++] = new ItemStack(Material.AIR);

		super.setContents(contents);
	}

}
